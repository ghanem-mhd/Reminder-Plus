package com.example.bassel.connectionadapter.requests;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.bassel.connectionadapter.ConnectionConfigs;
import com.example.bassel.connectionadapter.VolleyHandler;
import com.example.bassel.connectionadapter.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bassel on 11/18/2015.
 */
public class GetContactsInfoRequest extends com.android.volley.Request<String> {

    public static final String TAG = "GetContactsInfoRequest";
    private Response.Listener mListener;
    private Context mContext;
    private String session;
    private String[] contacts;

    /*
      *
     */
    public GetContactsInfoRequest(Context context,String session,
                                  String[] contacts,
                                  Response.Listener<JSONArray> responseListener,
                                  Response.ErrorListener errorListener){
        super(Method.POST,
                ConnectionConfigs.REST_SERVER_URL,
                errorListener);
        this.mListener = responseListener;
        this.mContext = context;
        this.contacts = contacts;
        this.session = session;
        VolleyHandler.setRetryPolicy(this);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        JSONArray contactsArray;
        contactsArray = new JSONArray();
        for(int i =0; i < this.contacts.length; i++)
            contactsArray.put(this.contacts[i]);

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("contacts", contactsArray);
            jsonBody.put("sessionId",this.session);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonBody.toString().getBytes();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String,String> headersMap = new HashMap<>();
        headersMap.put("Request-Type","GetContactsInfo");
        return headersMap;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    protected void onFinish(){
        super.onFinish();
        this.mListener = null;
    }

    @Override
    protected void deliverResponse(String response){
        if(mListener != null) try {
            JSONArray jsonResponse = new JSONArray(response);
            this.mListener.onResponse(jsonResponse);
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
            this.mListener.onResponse(null);
        }
    }


    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response){
        String parsed;
        try{
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        }catch(UnsupportedEncodingException e){
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    public void request(){
        VolleySingleton.getInstance(this.mContext).addToRequestQueue(this);
    }
}

