package com.example.bassel.connectionadapter;

/**
 * Created by Bassel on 10/25/2015.
 */
public abstract class ConnectionConfigs {
    public final static String REST_SERVER_URL = "http://nodejs-reminderplus.rhcloud.com/";
    public static final int SOCKET_TIME_OUT_MS = 10000;
    public static final int IMAGE_SOCKET_TIMEOUT_MS = 50000;
    public static final int RETRY = 2;
}
