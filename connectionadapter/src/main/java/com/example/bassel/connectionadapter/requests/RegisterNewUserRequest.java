package com.example.bassel.connectionadapter.requests;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.bassel.connectionadapter.ConnectionConfigs;
import com.example.bassel.connectionadapter.ModuleBuildConfigs;
import com.example.bassel.connectionadapter.VolleyHandler;
import com.example.bassel.connectionadapter.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bassel on 10/25/2015.
 */
public class RegisterNewUserRequest extends com.android.volley.Request<String> {

    public static final String TAG = "RegisterNewUserRequest";
    private Response.Listener mListener;
    private Context mContext;
    private String username;
    private String phoneNumber;
    private String email;
    private String imie;

    /*
      *
     */
    public RegisterNewUserRequest(Context context,String username, String phoneNumber,
                                  String email,String imie,
                                  Response.Listener<String> responseListener,
                                  Response.ErrorListener errorListener){
        super(Method.POST,
                ConnectionConfigs.REST_SERVER_URL,
                errorListener);
        this.mListener = responseListener;
        this.mContext = context;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.imie = imie;
        VolleyHandler.setRetryPolicy(this);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        String encodedName,encodedEmail,encodedPhoneNumber,encodedIMEI;
        try {
            encodedName = URLEncoder.encode(this.username,getParamsEncoding());
            encodedEmail = URLEncoder.encode(this.email,  getParamsEncoding());
            encodedPhoneNumber = URLEncoder.encode(this.phoneNumber, getParamsEncoding());
            encodedIMEI = URLEncoder.encode(this.imie, getParamsEncoding());
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
            return null;
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("name",encodedName);
            jsonBody.put("email",encodedEmail);
            jsonBody.put("phonenumber",encodedPhoneNumber);
            jsonBody.put("imie",encodedIMEI);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonBody.toString().getBytes();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String,String> headersMap = new HashMap<>();
        headersMap.put("Request-Type","Registeration");
        return headersMap;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    protected void onFinish(){
        super.onFinish();
        this.mListener = null;
    }

    @Override
    protected void deliverResponse(String response){
        if(mListener != null) try {
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.getBoolean("error"))
                this.mListener.onResponse(null);
            else
                this.mListener.onResponse(jsonResponse.getString("password"));
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
            this.mListener.onResponse(null);
        }
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response){
        String parsed;
        try{
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        }catch(UnsupportedEncodingException e){
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    public void request(){
        VolleySingleton.getInstance(this.mContext).addToRequestQueue(this);
    }
}
