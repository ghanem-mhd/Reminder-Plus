package com.example.bassel.connectionadapter;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

public abstract class VolleyHandler {

    private static final String TAG  = "HTTP_REQUEST";
    private static final String TAG2 = "HTTP_REQUEST_ERROR";

    public static void setRetryPolicy(com.android.volley.Request<String> request){
        request.setRetryPolicy(new DefaultRetryPolicy(ConnectionConfigs.SOCKET_TIME_OUT_MS,
                                                      ConnectionConfigs.RETRY,
                                                      DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public static void setUploadRequestRetryPolicy(Request<?> request){
        request.setRetryPolicy(new DefaultRetryPolicy(ConnectionConfigs.IMAGE_SOCKET_TIMEOUT_MS,
                ConnectionConfigs.RETRY,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public static void handleVolleyError(VolleyError error, String TAG){

            if (error instanceof TimeoutError) {
                Log.d(TAG, "TimeoutError");
            } else if (error instanceof  NoConnectionError) {
                Log.d(TAG, "NoConnectionError");
                Log.d(TAG, error.getMessage());
            } else if (error instanceof AuthFailureError) {
                Log.d(TAG,"AuthFailureError");
            } else if (error instanceof ServerError) {
                Log.d(TAG,"ServerError");
            } else if (error instanceof NetworkError) {
                Log.d(TAG, "NetworkError");
            } else if (error instanceof ParseError) {
                Log.d(TAG,error.toString());
                Log.d(TAG,error.getMessage());

                Log.d(TAG,"ParseError");
            }else
                Log.d(TAG ,"There is Error");

    }
}
