package com.example.bassel.connectionadapter.requests;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.bassel.connectionadapter.ConnectionConfigs;
import com.example.bassel.connectionadapter.VolleyHandler;
import com.example.bassel.connectionadapter.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bassel on 11/18/2015.
 */
public class LoginRequest extends com.android.volley.Request<String> {

    public static final String TAG = "LoginRequest";
    private Response.Listener mListener;
    private Context mContext;
    private String phoneNumber;
    private String password;

    /*
      *
     */
    public LoginRequest(Context context, String phoneNumber,
                                  String password,
                                  Response.Listener<String> responseListener,
                                  Response.ErrorListener errorListener){
        super(Method.POST,
                ConnectionConfigs.REST_SERVER_URL,
                errorListener);
        this.mListener = responseListener;
        this.mContext = context;
        this.phoneNumber = phoneNumber;
        this.password = password;
        VolleyHandler.setRetryPolicy(this);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        String encodedPhoneNumber,encodedPassword;
        try {
            encodedPassword = URLEncoder.encode(this.password,  getParamsEncoding());
            encodedPhoneNumber = URLEncoder.encode(this.phoneNumber, getParamsEncoding());
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
            return null;
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("phonenumber",encodedPhoneNumber);
            jsonBody.put("password",encodedPassword);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonBody.toString().getBytes();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String,String> headersMap = new HashMap<>();
        headersMap.put("Request-Type","Login");
        return headersMap;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    protected void onFinish(){
        super.onFinish();
        this.mListener = null;
    }

    @Override
    protected void deliverResponse(String response){
        if(mListener != null) try {
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.getBoolean("error"))
                this.mListener.onResponse(null);
            else
                this.mListener.onResponse(jsonResponse.getString("session"));
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
            this.mListener.onResponse(null);
        }
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response){
        String parsed;
        try{
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        }catch(UnsupportedEncodingException e){
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    public void request(){
        VolleySingleton.getInstance(this.mContext).addToRequestQueue(this);
    }
}
