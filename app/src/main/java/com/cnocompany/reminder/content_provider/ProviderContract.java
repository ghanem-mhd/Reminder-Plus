package com.cnocompany.reminder.content_provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class ProviderContract {

    public static final String PROVIDER_NAME = "com.cnocompany.reminder.content_provider.ReminderPlusProvider";

    public static abstract class ReminderContract implements BaseColumns {
        public static final String  TABLE_NAME           = "REMINDER";
        public static final Uri     CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/" + TABLE_NAME);
        public static final String  CONTENT_TYPE         = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;
        public static final String  CONTENT_ITEM_TYPE    = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;

        //location based or time based.
        public static final String TYPE_INT = "type";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "message";
        public static final String TIME        = "dateAndTime";
        // 1 for pending reminder 2 for accepted reminders
        public static final String STATUS = "reminderStatus";
        //when time/location of reminder trigger the reminder will still in DB but it has no alarm anymore
        public static final String HAS_ALARM_BOOL = "hasAlarm";
        public static final String SILENT_ALARM_BOOL   = "silentAlarm";


        //for location based reminder
        public static final String LAT      = "latitude";
        public static final String LNG      = "longitude";
        public static final String RADIUS   = "radius";

        //those fields fetch from backend
        public static final String BACKEND_CREATOR_ID = "backendCreateByUser_id";
        public static final String BACKEND_REMINDER_ID      = "backendReminder_id";
        public static final String BACKEND_USER_REMINDER_ID = "backendUserReminder_id";

        //when location reminder added the location may have an address and we got from place picker so we have to store it to show later in reminder details
        public static final String ADDRESS = "reminderAddress";


        public static final String PENDING_REMINDERS = "1";
        public static final int    TIME_REMINDER       = 1;
        public static final int    LOCATION_REMINDER   = 2;

        public static final int Pending_reminder_int = 1;
        public static final int Non_pending_reminder_int =  2;

    }

    public static abstract class ContactContract implements BaseColumns {
        public static final String  TABLE_NAME                  = "CONTACT";
        public static final Uri     CONTENT_URI         = Uri.parse("content://" + PROVIDER_NAME + "/" + TABLE_NAME);
        public static final String  CONTENT_TYPE                = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;
        public static final String  CONTENT_ITEM_TYPE           = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;


        public static final String LOCAL_NAME_STR   = "localName";
        public static final String RP_NAME_STR      = "rpName";
        public static final String PHONE_STR        = "phone";
        public static final String PHOTO_PATH       = "photo";
        public static final String RP_USER_BOOL     = "usingReminderPlus";
        public static final String BACKEND_USER_ID  = "backendUser_id";
        public static final String TRUSTED_CONTACT_BOOL  = "trustedContact";


    }

    public static abstract class ContactReminderContract implements BaseColumns {
        public static final String TABLE_NAME = "FRIEND_REMINDER";
        public static final Uri    CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/" + TABLE_NAME);
        public static final String CONTENT_TYPE         = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE    = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;


        public static final String BACKEND_REMINDER_ID = "reminder_id";
        public static final String BACKEND_CONTACT_ID = "contact_id";
        public static final String [] ProjectionCOLS = {_ID , BACKEND_REMINDER_ID, BACKEND_CONTACT_ID};



        public static String makeAlias(String column){
            return "FR_"+column;
        }
    }

    //alarm is an sub table of the reminder table created this class to query the alarms from content provider
    public static abstract class AlarmContract implements BaseColumns{
        public static final String TABLE_NAME = "ALARM";
        public static final Uri    CONTENT_URI          = Uri.parse("content://" + PROVIDER_NAME + "/" + TABLE_NAME);
        public static final String CONTENT_TYPE         = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE    = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;



        public static final String BACKEND_CREATE_BY_USER_ID    = ReminderContract.BACKEND_CREATOR_ID;
        public static final String ALARM_DESCRIPTION            = ReminderContract.DESCRIPTION;
        public static final String ALARM_TITLE                  = ReminderContract.TITLE;
        public static final String ALARM_TIME                   = ReminderContract.TIME;
        public static final String IS_SILENT                    = ReminderContract.SILENT_ALARM_BOOL;

    }

    public static abstract class LocationAlarmContract implements BaseColumns{
        public static final String TABLE_NAME = "LocationAlarm";
        public static final Uri    CONTENT_URI          = Uri.parse("content://" + PROVIDER_NAME + "/" + TABLE_NAME);
        public static final String CONTENT_TYPE         = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE    = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.nocompany.reminder."+TABLE_NAME;

        public static final String BACKEND_CREATE_BY_USER_ID    = ReminderContract.BACKEND_CREATOR_ID;
        public static final String DESCRIPTION            = ReminderContract.DESCRIPTION;
        public static final String TITLE                  = ReminderContract.TITLE;
        public static final String LAT                          = ReminderContract.LAT;
        public static final String LNG                          = ReminderContract.LNG;
        public static final String REMINDER_ID                  = ReminderContract._ID;
        public static final String RADIUS                       = ReminderContract.RADIUS;
    }

}
