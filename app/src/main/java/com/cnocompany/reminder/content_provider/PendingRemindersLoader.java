package com.cnocompany.reminder.content_provider;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

public class PendingRemindersLoader extends AsyncTask<Context,Void,String>{

    private OnPendingRemindersLoaderFinish onPendingRemindersLoaderFinish;


    public PendingRemindersLoader(OnPendingRemindersLoaderFinish onPendingRemindersLoaderFinish) {
        this.onPendingRemindersLoaderFinish = onPendingRemindersLoaderFinish;
    }

    @Override
    protected String doInBackground(Context... params) {
        Context context = params[0];
        String selection = ProviderContract.ReminderContract.STATUS + " = " + ProviderContract.ReminderContract.PENDING_REMINDERS;
        Cursor cursor = context.getContentResolver().query(ProviderContract.ReminderContract.CONTENT_URI,null,selection,null,null);
        return String.valueOf(cursor.getCount());
    }

    @Override
    protected void onPostExecute(String pendingReminderCount) {
        onPendingRemindersLoaderFinish.OnFinish(pendingReminderCount);
    }

    public interface OnPendingRemindersLoaderFinish{
        void OnFinish(String pendingRminderCount);
    }
}
