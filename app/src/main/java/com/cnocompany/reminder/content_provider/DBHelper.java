package com.cnocompany.reminder.content_provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ReminderPlusDB.db";


    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE       = " REAL";
    private static final String COMMA_SEP = ",";

    //all boolean value are integer 1 mean true and 0 mean false
    //type value is also integer 1 for time reminder an 2 for location reminder

    private static final String SQL_CREATE_REMINDER_TABLE =
            "CREATE TABLE " +
                    ProviderContract.ReminderContract.TABLE_NAME + " (" +
                    ProviderContract.ReminderContract._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ProviderContract.ReminderContract.TYPE_INT + INTEGER_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.TITLE + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.TIME + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.HAS_ALARM_BOOL + INTEGER_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.LAT    + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.LNG    + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.RADIUS + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.BACKEND_CREATOR_ID + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.BACKEND_REMINDER_ID + TEXT_TYPE  +" UNIQUE "+ COMMA_SEP +
                    ProviderContract.ReminderContract.BACKEND_USER_REMINDER_ID + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.STATUS + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.SILENT_ALARM_BOOL + INTEGER_TYPE + COMMA_SEP +
                    ProviderContract.ReminderContract.ADDRESS + TEXT_TYPE +
                    " );";
    private static final String SQL_CREATE_CONTACT_TABLE =
            "CREATE TABLE " +
                    ProviderContract.ContactContract.TABLE_NAME + " (" +
                    ProviderContract.ContactContract._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ProviderContract.ContactContract.LOCAL_NAME_STR     + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ContactContract.PHONE_STR          + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ContactContract.PHOTO_PATH         + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ContactContract.RP_NAME_STR        + TEXT_TYPE + COMMA_SEP +
                    ProviderContract.ContactContract.RP_USER_BOOL       + INTEGER_TYPE + COMMA_SEP +
                    ProviderContract.ContactContract.TRUSTED_CONTACT_BOOL + INTEGER_TYPE + COMMA_SEP +
                    ProviderContract.ContactContract.BACKEND_USER_ID    + INTEGER_TYPE +" UNIQUE "+
                    " );";
    private static final String SQL_CREATE_CONTACT_REMINDER_TABLE =
            "CREATE TABLE " +
                    ProviderContract.ContactReminderContract.TABLE_NAME + " (" +
                    ProviderContract.ContactReminderContract._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ProviderContract.ContactReminderContract.BACKEND_CONTACT_ID + INTEGER_TYPE + COMMA_SEP +
                    ProviderContract.ContactReminderContract.BACKEND_REMINDER_ID + INTEGER_TYPE + COMMA_SEP +

                    "FOREIGN KEY(" + ProviderContract.ContactReminderContract.BACKEND_CONTACT_ID + ") REFERENCES " +
                    ProviderContract.ContactContract.TABLE_NAME + "(" + ProviderContract.ContactContract._ID + "), " +

                    "FOREIGN KEY(" + ProviderContract.ContactReminderContract.BACKEND_REMINDER_ID + ") REFERENCES " +
                    ProviderContract.ReminderContract.TABLE_NAME + "(" + ProviderContract.ReminderContract._ID +
                    "))";


    private static final String SQL_DELETE_REMINDER_TABLE =
            "DROP TABLE IF EXISTS " + ProviderContract.ReminderContract.TABLE_NAME;
    private static final String SQL_DELETE_FRIEND_TABLE =
            "DROP TABLE IF EXISTS " + ProviderContract.ContactContract.TABLE_NAME;
    private static final String SQL_DELETE_FRIEND_REMINDER_TABLE =
            "DROP TABLE IF EXISTS " + ProviderContract.ContactReminderContract.TABLE_NAME;

    private static DBHelper sInstance;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    public static synchronized DBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_REMINDER_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CONTACT_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CONTACT_REMINDER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL(SQL_DELETE_REMINDER_TABLE);
        sqLiteDatabase.execSQL(SQL_DELETE_FRIEND_TABLE);
        sqLiteDatabase.execSQL(SQL_DELETE_FRIEND_REMINDER_TABLE);
        onCreate(sqLiteDatabase);
    }
}
