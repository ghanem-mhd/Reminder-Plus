package com.cnocompany.reminder.content_provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.HashMap;


public class ReminderPlusProvider extends ContentProvider  {

    private static final int REMINDER_LIST          = 1;
    private static final int REMINDER_ID            = 2;

    private static final int CONTACT_LIST           = 3;
    private static final int CONTACT_ID             = 4;

    private static final int CONTACTS_REMINDER_LIST = 5;
    private static final int CONTACT_REMINDER_ID    = 6;

    private static final int ALARM_LIST             = 7;
    private static final int ALARM_ALARM_ID         = 9;

    private static final int LOCATION_ALARM_LIST    = 10;
    private static final int LOCATION_ALARM__ID     = 11;



    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.ReminderContract.TABLE_NAME, REMINDER_LIST);
        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.ReminderContract.TABLE_NAME + "/#", REMINDER_ID);

        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.ContactContract.TABLE_NAME, CONTACT_LIST);
        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.ContactContract.TABLE_NAME + "/#", CONTACT_ID);


        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.ContactReminderContract.TABLE_NAME, CONTACTS_REMINDER_LIST);
        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.ContactReminderContract.TABLE_NAME + "/#", CONTACT_REMINDER_ID);

        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.AlarmContract.TABLE_NAME, ALARM_LIST);
        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.AlarmContract.TABLE_NAME + "/#", ALARM_ALARM_ID);


        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.LocationAlarmContract.TABLE_NAME, LOCATION_ALARM_LIST);
        URI_MATCHER.addURI(ProviderContract.PROVIDER_NAME, ProviderContract.LocationAlarmContract.TABLE_NAME + "/#", LOCATION_ALARM__ID);

    }

    private  SQLiteOpenHelper dbhelper;

    private HashMap<String, String> setUpProjectionMap() {
        HashMap<String,String> stringStringHashMap = new HashMap<>();
            stringStringHashMap.put("REMINDER._ID","REMINDER._ID AS _ID");
            stringStringHashMap.put("CONTACT._ID","CONTACT._ID AS CONTACT_ID");
        return stringStringHashMap;
    }

    @Override
    public boolean onCreate() {
        dbhelper =  new DBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor cursor = null;
        switch (URI_MATCHER.match(uri)) {
            case REMINDER_LIST:
                queryBuilder.setTables(ProviderContract.ReminderContract.TABLE_NAME + " LEFT JOIN "+ProviderContract.ContactContract.TABLE_NAME + " ON (REMINDER.backendCreateByUser_id = CONTACT.backendUser_id)");
                cursor = queryBuilder.query(dbhelper.getReadableDatabase(), projection, selection,selectionArgs, null, null, sortOrder);
                break;
            case REMINDER_ID:
                queryBuilder.setTables(ProviderContract.ReminderContract.TABLE_NAME + " LEFT JOIN "+ProviderContract.ContactContract.TABLE_NAME + " ON (REMINDER.backendCreateByUser_id = CONTACT.backendUser_id)");
                queryBuilder.appendWhere("REMINDER."+ProviderContract.ReminderContract._ID + " = " + uri.getLastPathSegment());
                cursor = queryBuilder.query(dbhelper.getReadableDatabase(),projection,selection,selectionArgs,null,null,sortOrder);
                break;

            case CONTACT_LIST:
                queryBuilder.setTables(ProviderContract.ContactContract.TABLE_NAME);
                cursor = queryBuilder.query(dbhelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);

                break;

            case CONTACT_ID:
                queryBuilder.setTables(ProviderContract.ContactContract.TABLE_NAME);
                queryBuilder.appendWhere(ProviderContract.ContactContract._ID + " = " + uri.getLastPathSegment());
                break;


            case CONTACTS_REMINDER_LIST:
                queryBuilder.setTables(ProviderContract.ContactReminderContract.TABLE_NAME);
                cursor = queryBuilder.query(dbhelper.getReadableDatabase(), projection,selection   , selectionArgs, null, null, sortOrder);
                break;



            case ALARM_LIST:
                // we will get alarm from reminder table with some projection
                queryBuilder.setTables(ProviderContract.ReminderContract.TABLE_NAME);
                projection = new String [] {
                        ProviderContract.ReminderContract._ID,
                        ProviderContract.ReminderContract.BACKEND_CREATOR_ID,
                        ProviderContract.ReminderContract.DESCRIPTION,
                        ProviderContract.ReminderContract.TITLE,
                        ProviderContract.ReminderContract.TIME,
                        ProviderContract.ReminderContract.HAS_ALARM_BOOL,
                        ProviderContract.ReminderContract.SILENT_ALARM_BOOL,
                        ProviderContract.ReminderContract.TYPE_INT
                };


                cursor = queryBuilder.query(dbhelper.getReadableDatabase(),projection,selection,selectionArgs,null,null,sortOrder);
                break;

            case LOCATION_ALARM_LIST:
                // we will get location alarm from reminder table with some projection
                queryBuilder.setTables(ProviderContract.ReminderContract.TABLE_NAME);
                projection = new String [] {
                        ProviderContract.ReminderContract._ID,
                        ProviderContract.ReminderContract.BACKEND_CREATOR_ID,
                        ProviderContract.ReminderContract.LNG,
                        ProviderContract.ReminderContract.LAT,
                        ProviderContract.ReminderContract.RADIUS,
                        ProviderContract.ReminderContract.TYPE_INT,
                        ProviderContract.ReminderContract.TITLE,
                        ProviderContract.ReminderContract.DESCRIPTION
                };
                String where = ProviderContract.ReminderContract.HAS_ALARM_BOOL + " = 1 AND "+ ProviderContract.ReminderContract.TYPE_INT + " = 2";
                queryBuilder.appendWhere(where);
                cursor = queryBuilder.query(dbhelper.getReadableDatabase(),projection,selection,selectionArgs,null,null,sortOrder);
                break;


        }
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case  REMINDER_LIST :
                return ProviderContract.ReminderContract.CONTENT_TYPE;

            case  REMINDER_ID :
                return ProviderContract.ReminderContract.CONTENT_ITEM_TYPE;

            case CONTACT_LIST:
                return ProviderContract.ContactContract.CONTENT_TYPE;

            case CONTACT_ID:
                return ProviderContract.ContactContract.CONTENT_ITEM_TYPE;

            case CONTACTS_REMINDER_LIST:
                return ProviderContract.ContactReminderContract.CONTENT_TYPE;

            case CONTACT_REMINDER_ID:
                return ProviderContract.ContactReminderContract.CONTENT_ITEM_TYPE;

        }

        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase database = dbhelper.getWritableDatabase();
        long id ;
        Uri _uri = null;
        switch (URI_MATCHER.match(uri)){
            case REMINDER_LIST:
                id =  database.insert(ProviderContract.ReminderContract.TABLE_NAME,null,contentValues);
                if (id > 0){
                    _uri = ContentUris.withAppendedId(ProviderContract.ReminderContract.CONTENT_URI, id);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;

            case CONTACT_LIST:
                id =  database.insert(ProviderContract.ContactContract.TABLE_NAME,null,contentValues);
                if (id > 0)
                {
                    _uri = ContentUris.withAppendedId(ProviderContract.ReminderContract.CONTENT_URI, id);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                if (id == -1)
                {
                    id =  database.update(ProviderContract.ContactContract.TABLE_NAME,contentValues,"_ID=?", new String[]{String.valueOf(id)});
                    _uri = ContentUris.withAppendedId(ProviderContract.ReminderContract.CONTENT_URI, id);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;

            case CONTACTS_REMINDER_LIST:

                id =  database.insert(ProviderContract.ContactReminderContract.TABLE_NAME,null,contentValues);
                if (id > 0){
                    _uri = ContentUris.withAppendedId(ProviderContract.ReminderContract.CONTENT_URI, id);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
        }
        return _uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int deletedCount = 0;
        switch (URI_MATCHER.match(uri)){
            case REMINDER_LIST:
                deletedCount = dbhelper.getWritableDatabase().delete(ProviderContract.ReminderContract.TABLE_NAME, selection, selectionArgs);
                break;

            case CONTACT_LIST:
                deletedCount = dbhelper.getWritableDatabase().delete(ProviderContract.ContactContract.TABLE_NAME, selection, selectionArgs);
                break;

            case CONTACT_REMINDER_ID:
                deletedCount = dbhelper.getWritableDatabase().delete(ProviderContract.ContactReminderContract.TABLE_NAME,selection,selectionArgs);
                break;
        }

        if (deletedCount>0){
            getContext().getContentResolver().notifyChange(uri,null);
        }

        return deletedCount;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        int updatedCount = 0;
        switch (URI_MATCHER.match(uri)){
            case REMINDER_LIST:
                updatedCount = dbhelper.getWritableDatabase().update(ProviderContract.ReminderContract.TABLE_NAME,contentValues,selection,selectionArgs);
                break;

            case CONTACT_LIST:
                updatedCount = dbhelper.getWritableDatabase().update(ProviderContract.ContactContract.TABLE_NAME,contentValues,selection,selectionArgs);
                break;

            case CONTACT_REMINDER_ID:
                updatedCount = dbhelper.getWritableDatabase().update(ProviderContract.ContactReminderContract.TABLE_NAME,contentValues,selection,selectionArgs);
                break;
        }

        if (updatedCount>0){
            getContext().getContentResolver().notifyChange(uri,null);
        }

        return updatedCount;
    }
}
