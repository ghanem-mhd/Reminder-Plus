package com.cnocompany.reminder.list_adapter;

import com.cnocompany.reminder.date_objects.Reminder;

public interface NewsFeedInterface {
    public void onNewFeedItemClick(Reminder clickedReminder);
}
