package com.cnocompany.reminder.list_adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.date_objects.Reminder;

import java.text.ParseException;
import java.util.Date;

public class PendingReminderCursorAdapter extends CursorAdapter{

    private Context context;

    public PendingReminderCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.pending_reminder_item_list_layout,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //todo photo universal
        ImageView creatorPhoto = (ImageView) view.findViewById(R.id.pending_reminder_creator_photo);
        TextView creatorNameDescription = (TextView) view.findViewById(R.id.pending_reminder_name_description);
        TextView title = (TextView) view.findViewById(R.id.pending_reminder_title);
        TextView time_date_location = (TextView) view.findViewById(R.id.pending_reminder_time_date_location);

        Reminder reminder  = Reminder.getReminderObjectFromCursor(cursor);


        if (reminder != null) {
            creatorNameDescription.setText(String.format("%s - %s", reminder.getCreatorName(), reminder.getDescription()));
            if (reminder.getTitle() != null){
                title.setText(reminder.getTitle());
            }else {
                title.setVisibility(View.GONE);
            }
        }





        if (reminder.getType() == ProviderContract.ReminderContract.LOCATION_REMINDER){
            if (reminder.getAddress() !=null ){
                time_date_location.setText(reminder.getAddress());
            }else {
                time_date_location.setVisibility(View.GONE);
            }

        }else{
                if (reminder.getTime() != null){
                        try {
                        Date reminderDate = Constant.DB_formatter.parse(reminder.getTime());
                        CharSequence datePartString = DateUtils.getRelativeDateTimeString(context.getApplicationContext(), reminderDate.getTime(), DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS, DateUtils.FORMAT_ABBREV_WEEKDAY);
                        time_date_location.setText(datePartString.toString());
                    } catch (ParseException e) {e.printStackTrace();}
                }else {
                    time_date_location.setVisibility(View.GONE);
                }

        }





    }
}

