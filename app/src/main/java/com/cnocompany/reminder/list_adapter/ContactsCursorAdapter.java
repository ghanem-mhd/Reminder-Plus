package com.cnocompany.reminder.list_adapter;


import android.content.Context;
import android.database.Cursor;

import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.R;
import com.rey.material.widget.Button;

public class ContactsCursorAdapter extends CursorAdapter {
    private static final String LOG_TAG = "contactsCursor";
    private Context context;
    public ContactsCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.rp_contacts_list_item, parent, false);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name        = (TextView) view.findViewById(R.id.contact_item_name);
        TextView number      = (TextView) view.findViewById(R.id.contact_item_number);
        ImageView imageView  = (ImageView) view.findViewById(R.id.contact_item_photo);
        TextView trustedIcon = (TextView) view.findViewById(R.id.contact_item_trusted_icon);

        name.setText(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)));
        number.setText(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHONE_STR)));

        int usingReminderPlus = cursor.getInt(cursor.getColumnIndex(ProviderContract.ContactContract.RP_USER_BOOL));
        int trustedContact    = cursor.getInt(cursor.getColumnIndex(ProviderContract.ContactContract.TRUSTED_CONTACT_BOOL));

        //using reminder plus
        if (usingReminderPlus == 1){
            //Picasso.with(context).load(Uri.parse(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHOTO_PATH)))).into(imageView);
            if (trustedContact == 1){
                trustedIcon.setVisibility(View.VISIBLE);
            }else {
                trustedIcon.setVisibility(View.GONE);
            }
        }else{
            Button inviteButton = (Button) view.findViewById(R.id.contact_item_invite_button);
            inviteButton.setVisibility(View.VISIBLE);
            inviteButton.setTag(cursor.getInt(cursor.getColumnIndex(ProviderContract.ContactContract.BACKEND_USER_ID)));
            inviteButton.setOnClickListener(onInviteButtonClick);
        }


    }

    private View.OnClickListener onInviteButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Integer integer = (Integer) view.getTag();
            Log.d(LOG_TAG,integer.toString());
        }
    };
}
