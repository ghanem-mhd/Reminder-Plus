package com.cnocompany.reminder.list_adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cnocompany.reminder.R;
import com.cnocompany.reminder.date_objects.Event;
import com.cnocompany.reminder.date_objects.Reminder;
import com.rey.material.widget.Button;

import java.util.ArrayList;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder>{


    private ArrayList<Reminder> reminders;
    private NewsFeedInterface newsFeedInterface;
    public NewsFeedAdapter(Context context,NewsFeedInterface newsFeedInterface) {
        this.newsFeedInterface = newsFeedInterface;
    }

    @Override
    public NewsFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_feed_item_list, parent, false);
        return new NewsFeedViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NewsFeedViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        if (reminders != null)
            return reminders.size();
        else
            return 0;
    }

    public class NewsFeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView creatorPhoto ;
        TextView creatorNameDescription ;
        TextView title ;
        TextView time_date_location ;
        RelativeLayout relativeLayout;

        public NewsFeedViewHolder(View itemView) {
            super(itemView);

             creatorPhoto = (ImageView) itemView.findViewById(R.id.news_feed_item_creator_photo);
             creatorNameDescription = (TextView) itemView.findViewById(R.id.news_feed_item_description);
             title = (TextView) itemView.findViewById(R.id.news_feed_item_title);
             time_date_location = (TextView) itemView.findViewById(R.id.news_feed_item_time_date_location);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.news_feed_item_layout);


            relativeLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            // no position because the layout in updating
            if (position != RecyclerView.NO_POSITION) {
                switch (v.getId()){
                    case R.id.news_feed_item_layout:
                        newsFeedInterface.onNewFeedItemClick(reminders.get(position));
                        break;
                }
            }



        }
    }
}
