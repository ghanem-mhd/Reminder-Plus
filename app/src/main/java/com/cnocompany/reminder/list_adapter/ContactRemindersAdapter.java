package com.cnocompany.reminder.list_adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.date_objects.Reminder;

import java.text.ParseException;
import java.util.ArrayList;

public class ContactRemindersAdapter extends BaseAdapter{

    private ArrayList<Reminder> data = new ArrayList<>();
    private Context context;

    public ContactRemindersAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return data.get(i).getBackendReminderID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if (view == null){
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.reminder_list_item, viewGroup, false);
            // well set up the ViewHolder
            viewHolder = new ViewHolder();
            viewHolder.title    = (TextView) view.findViewById(R.id.calendar_reminder_item_title);
            viewHolder.time     = (TextView) view.findViewById(R.id.calendar_reminder_item_time);
            viewHolder.creator  = (TextView) view.findViewById(R.id.calendar_reminder_item_creator_name);
            // store the holder with the view.
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Reminder reminder = (Reminder) getItem(i);

        CharSequence relative = null;
        try {
            relative = Constant.ONLY_TIME_FORMATTER.format(Constant.DB_formatter.parse(reminder.getTime()));
            viewHolder.title.setText(reminder.getTitle());
            viewHolder.time.setText("at " + relative);
            viewHolder.creator.setText("tt");
        } catch (ParseException e) {
            e.printStackTrace();
        }




        return view;
    }

    public void replaceDate(ArrayList<Reminder> reminders){
        data.clear();
        data.addAll(reminders);
    }

    public void appendDate(ArrayList<Reminder> reminders){
        data.addAll(reminders);
    }

    static class ViewHolder{
        TextView title;
        TextView time;
        TextView creator;
    }
}
