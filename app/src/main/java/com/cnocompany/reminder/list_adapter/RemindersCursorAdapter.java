package com.cnocompany.reminder.list_adapter;



import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UserInfoPreferences;

import java.text.ParseException;


public class RemindersCursorAdapter extends CursorAdapter{
    private Context mContext;
    private long userBackend_ID;

    public RemindersCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.mContext = context;
        //todo chane defult value to another one
        userBackend_ID = UserInfoPreferences.getInstance(context).getLong(UserInfoPreferences.USER_BACKEND_ID_LONG,2);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.reminder_list_item,viewGroup,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView title          = (TextView) view.findViewById(R.id.calendar_reminder_item_title);
        TextView time           = (TextView) view.findViewById(R.id.calendar_reminder_item_time);
        TextView createdByName  = (TextView) view.findViewById(R.id.calendar_reminder_item_creator_name);

        String date = cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.TIME));
        CharSequence relative = null;
        try {
            relative = Constant.ONLY_TIME_FORMATTER.format(Constant.DB_formatter.parse(date));
            title.setText(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.TITLE)));
            time.setText("at " + relative);

            if (cursor.getLong(cursor.getColumnIndex(ProviderContract.ReminderContract.BACKEND_CREATOR_ID))==userBackend_ID){
                createdByName.setText("By You");
            }else
                createdByName.setText(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)));


        } catch (ParseException e) {
            e.printStackTrace();
        }





    }



}
