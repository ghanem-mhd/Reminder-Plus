package com.cnocompany.reminder.utilises;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.cnocompany.reminder.helper.CSharedPreferences;




public class SyncService extends Service{
    private static final String TAG = "SyncService";


    @Override
    public void onCreate() {
        super.onCreate();
        //database = new DAO(getBaseContext());

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {



        Log.d(TAG,"from start Command");
        CSharedPreferences cSharedPreferences = new CSharedPreferences(CSharedPreferences.USER_INFO, getBaseContext());
        String session_id = cSharedPreferences.getString(CSharedPreferences.SESSION_ID, null);

       if (session_id != null){
           fetchFriendRequestByMe(session_id);
           fetchFriendRequestToMe(session_id);
           fetchPendingReminders(session_id);
       }
        return super.onStartCommand(intent, flags, startId);
    }

    private void fetchFriendRequestByMe(String session_id){

    }

    private void fetchFriendRequestToMe(String session_id){

    }

    private void fetchPendingReminders(String session_id) {

    }

}
