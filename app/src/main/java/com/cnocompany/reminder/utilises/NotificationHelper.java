package com.cnocompany.reminder.utilises;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.cnocompany.reminder.date_objects.Reminder;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.main_uI.MainActivity;

/**
 * Created by Mo7ammed on 6/7/2015.
 */
public class NotificationHelper {

    public static void showReminderNotification(Reminder reminder,Context context){
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSound(soundUri)
                        .setSmallIcon(R.drawable.ic_launcher_calendar)
                        .setContentTitle("New Reminder From "+reminder.getBackendReminderID())
                        .setContentText(reminder.getTitle());

        Intent resultIntent = new Intent(context,MainActivity.class);
        resultIntent.putExtra("TAG", "reminders");
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public static void showFriendRequestReminder(){

    }
}
