package com.cnocompany.reminder.utilises;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public abstract class SyncManager {
    private final static long SYNC_INTERVAL_MILLIS = 1000*10;

    public static void setSyncService(Context context){
        Intent intent = new Intent(context, SyncService.class);
        PendingIntent recurringSync = PendingIntent.getService(context, -1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                0,324, recurringSync);
    }


    public static void stopSyncSevice(Context context){
        Intent intent = new Intent(context, SyncService.class);
        PendingIntent recurringSync = PendingIntent.getService(context, -1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(recurringSync);
    }
}
