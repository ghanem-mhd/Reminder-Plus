package com.cnocompany.reminder.registration;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cnocompany.reminder.helper.PhotoManager;
import com.cnocompany.reminder.helper.TouchImageView;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RegistrationFragmentStep3 extends android.app.Fragment implements View.OnClickListener{
    private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 1;
    private continueButtonListener mCallback;
    private static final String LOG_TAG = "RP_reg_step3";
    private static final int PICK_IMAGE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;


    @Bind(R.id.user_name) EditText nameInput ;
    @Bind(R.id.registration_continue_button)Button continueButton ;
    @Bind(R.id.contact_item_photo)TouchImageView userPhoto ;
    @Bind(R.id.select_photo) com.rey.material.widget.Button selectButton;


    private UserInfoPreferences userInfoPreferences;
    private Uri outputFileUri;
    private File photoFile;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoPreferences = UserInfoPreferences.getInstance(getActivity().getApplicationContext());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.regeistration_layout_step_3, container, false);


        ButterKnife.bind(this, rootView);


        selectButton.setOnClickListener(this);
        continueButton.setOnClickListener(this);

        nameInput.setFocusableInTouchMode(true);
        nameInput.requestFocus();

        hideKeyboard(rootView);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.registration_continue_button:
                if (TextUtils.isEmpty(nameInput.getText())){
                    Toast.makeText(getActivity().getApplicationContext(),getString(R.string.no_name_waring),Toast.LENGTH_LONG).show();
                    return;
                }else {
                    userInfoPreferences.put(UserInfoPreferences.USER_REMOTE_NAME, nameInput.getText().toString().trim());
                    if (outputFileUri != null){ //the user want photo
                        userInfoPreferences.put(UserInfoPreferences.USER_PHOTO_PATH,outputFileUri.toString());
                    }
                    mCallback.onStepThreeContinueButtonClicked();
                }
                break;

            case R.id.select_photo:
                openImageIntent();
                break;

        }
    }

    public interface continueButtonListener {
        void onStepThreeContinueButtonClicked();
    }

    @Override
    public void onAttach(Activity Context) {
        super.onAttach(Context);
        try {
            mCallback = (continueButtonListener) Context;
        } catch (ClassCastException e) {
            throw new ClassCastException(Context.toString() + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == YOUR_SELECT_PICTURE_REQUEST_CODE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }
                Uri selectedImageUri;
                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data.getData();
                    savePhoto(selectedImageUri);
                }
                userPhoto.setScaleType(ImageView.ScaleType.CENTER);
                Picasso.with(getActivity()).load(selectedImageUri).memoryPolicy(MemoryPolicy.NO_CACHE,MemoryPolicy.NO_STORE).into(userPhoto);
            }
        }
    }

    private void savePhoto(Uri uri) {
        try {
            InputStream iStream =   getActivity().getContentResolver().openInputStream(uri);
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            int len = 0;
            while ((len = iStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            OutputStream out = new FileOutputStream(photoFile);
            out.write(byteBuffer.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void openImageIntent() {
        try {
            // Determine Uri of camera image to save.

            photoFile = PhotoManager.createImageFile(-1);
            outputFileUri = Uri.fromFile(photoFile);
            // Camera.
            final List<Intent> intents = new ArrayList<Intent>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = getActivity().getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for(ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                intents.add(intent);
            }


            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");

            intents.add(pickIntent);

            // Filesystem.
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");

            // Chooser of filesystem options.
            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");






            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Parcelable[intents.size()]));

            if (chooserIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
