package com.cnocompany.reminder.registration;



import android.accounts.Account;


import android.accounts.AccountManager;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;


import com.cnocompany.reminder.authenticator.AccountConstants;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.main_uI.EditUserInfoActivity;


public class RegistrationActivity extends com.cnocompany.reminder.registration.AccountAuthenticatorActivity implements
        RegistrationFragmentStep1.continueButtonListener,
        RegistrationFragmentStep2.continueButtonListener{

    public static final String LOG_TAG      = "RP_ind_auth";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String AUTH_TOKEN   = "authToken";
    private static final int INFO_REQ = 1;


    private AccountManager accountManager;
    private FragmentManager fragmentManager = getFragmentManager();

    private SharedPreferences.Editor editor;

    private Fragment fragment;
    private Toolbar toolbar;

    private String phoneNumber;
    private String authToken;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            phoneNumber = savedInstanceState.getString(PHONE_NUMBER);
            authToken   = savedInstanceState.getString(AUTH_TOKEN);
        }

        setContentView(R.layout.regeistration_layout);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        accountManager = AccountManager.get(this);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        editor = preferences.edit();


        phoneNumber = preferences.getString(PHONE_NUMBER, null);
        authToken   = preferences.getString(AUTH_TOKEN, null);

        if (fragment == null) {

            if (phoneNumber == null) {
                fragment = new RegistrationFragmentStep1();
            } else {
                if (authToken == null) {
                    fragment = new RegistrationFragmentStep2();
                } else {
                    fragment = new RegistrationFragmentStep3();
                }
            }
            fragmentManager.beginTransaction().replace(R.id.registration_container, fragment).commit();
        }
    }


    @Override
    public void onStepOneContinueButtonClicked(String phoneNumber) {
        //editor.putString(PHONE_NUMBER,phoneNumber);
       // editor.commit();
        this.phoneNumber = phoneNumber;
        fragment = new RegistrationFragmentStep2();
        fragmentManager.beginTransaction().replace(R.id.registration_container, fragment).commit();
    }

    @Override
    public void onStepTwoContinueButtonClicked(String authToken) {
        editor.putString(AUTH_TOKEN,authToken);
        editor.commit();
        this.authToken = authToken;
        requstUserInfo();
    }



    void finishingRegistration(String phoneNumber,String authToken){
        String accountName = getString(R.string.individual_account_name) + " - " + phoneNumber;
        Account account = new Account(accountName, AccountConstants.ACCOUNT_TYPE);
        accountManager.addAccountExplicitly(account, null, null);
        accountManager.setAuthToken(account, AccountConstants.AUTH_TOKEN_IND, authToken);
        Bundle data = new Bundle();
        data.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
        data.putString(AccountManager.KEY_ACCOUNT_TYPE, AccountConstants.ACCOUNT_TYPE);
        data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
        setAccountAuthenticatorResult(data);
        setResult(RESULT_OK);
        finish();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER, phoneNumber);
        outState.putString(AUTH_TOKEN, authToken);
        super.onSaveInstanceState(outState);
    }

    private void requstUserInfo() {
        Intent intent = new Intent(this,EditUserInfoActivity.class);
        intent.putExtra(EditUserInfoActivity.REG_REQ_ARG, true);
        startActivityForResult(intent, INFO_REQ);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == INFO_REQ){
                finishingRegistration(phoneNumber,authToken);
            }
        }else {
            requstUserInfo();
        }
    }
}
