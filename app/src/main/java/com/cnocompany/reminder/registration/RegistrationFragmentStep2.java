package com.cnocompany.reminder.registration;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cnocompany.reminder.R;



public class RegistrationFragmentStep2 extends Fragment implements View.OnClickListener {
    private static final String LOG_TAG         = "rp_reg_step2";
    private static final int    TIMER_DURATION  = 300000;


    private continueButtonListener mCallback;

    private TextView timerTextView;
    private EditText codeInput;
    private Button resendCodeButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.regeistration_layout_step_2, container, false);


        timerTextView       = (TextView) rootView.findViewById(R.id.timer_textview);
        codeInput           = (EditText) rootView.findViewById(R.id.code_input);
        Button continueButton = (Button) rootView.findViewById(R.id.registration_continue_button);
        resendCodeButton    = (Button) rootView.findViewById(R.id.resend_button);


        continueButton.setOnClickListener(this);
        resendCodeButton.setOnClickListener(this);
        setTimer();
        showKeyboard();

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.registration_continue_button:
                if (!TextUtils.isEmpty(codeInput.getText())){
                    //TODO send the code and receive auth token (Auto Generated Password -_- ).
                    mCallback.onStepTwoContinueButtonClicked("sdf");
                }
                else Toast.makeText(getActivity(),R.string.registration_no_code,Toast.LENGTH_LONG).show();

                break;

            case R.id.resend_button:
                //TODO resend the code
                resendCodeButton.setEnabled(false);
                setTimer();
                break;
        }
    }


    public interface continueButtonListener {
        void onStepTwoContinueButtonClicked(String authToken);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (continueButtonListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    private void setTimer(){

        new CountDownTimer(TIMER_DURATION, 1000) {

            public void onTick(long millisUntilFinished) {
                timerTextView.setText(getTime(millisUntilFinished));
            }

            public void onFinish() {
                resendCodeButton.setEnabled(true);
                timerTextView.setText(getTime(0));

            }

            private String getTime(long millis){
                int seconds = (int) (millis / 1000) % 60;
                int minutes = (int) ((millis / (1000 * 60)) % 60);
                return String.format("%02d:%02d", minutes, seconds);
            }
        }.start();
    }

    private void showKeyboard(){

        codeInput.setFocusableInTouchMode(true);
        codeInput.requestFocus();
    }



}