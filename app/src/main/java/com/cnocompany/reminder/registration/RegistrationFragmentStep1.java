package com.cnocompany.reminder.registration;


import android.app.Activity;
import android.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.EditText;

import com.cnocompany.reminder.helper.Country;
import com.cnocompany.reminder.R;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.rey.material.widget.Button;


import java.util.Locale;


public class RegistrationFragmentStep1 extends Fragment implements View.OnClickListener{
    public static final String LOG_TAG = "registration_step1";
    private continueButtonListener mCallback;
    private static final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();


    private Button countryName;
    private EditText countryCodeInput;
    private EditText phoneNumberInput;

    private Country selectedCountry;

    public interface continueButtonListener {
        void onStepOneContinueButtonClicked(String phoneNumber);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.regeistration_layout_step_1, container, false);

        countryName         = (Button) rootView.findViewById(R.id.country_name);
        countryCodeInput    = (EditText) rootView.findViewById(R.id.country_code_input);
        phoneNumberInput    = (EditText) rootView.findViewById(R.id.phone_number_input);
        android.widget.Button continueButton = (android.widget.Button) rootView.findViewById(R.id.registration_continue_button);

        phoneNumberInput.setFocusableInTouchMode(true);
        phoneNumberInput.requestFocus();
        continueButton.setOnClickListener(this);
        countryName.setOnClickListener(this);
        setUserCountry();
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (continueButtonListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.registration_continue_button:
                submitPhoneNumber();
                break;

            case R.id.country_name:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivityForResult(new Intent(getActivity(), com.cnocompany.reminder.registration.CountryPicker.class), com.cnocompany.reminder.registration.CountryPicker.PICK_COUNTRY_REQ);
                    }
                }, 250);
                break;
        }
    }

    private boolean validPhoneNumber(String phoneNumber) {
        try
        {
            Phonenumber.PhoneNumber number = phoneUtil.parse(phoneNumber,selectedCountry.getShortCut());
            return phoneUtil.isValidNumberForRegion(number, selectedCountry.getShortCut().toUpperCase());
        }
        catch (NumberParseException e) {
            e.printStackTrace();
            return false;
        }
    }


    //TODO
    public void submitPhoneNumber()
    {
        String phoneNumber = phoneNumberInput.getText().toString();
        if (validPhoneNumber(phoneNumber)) {
            mCallback.onStepOneContinueButtonClicked(phoneNumber);
        }
        else Toast.makeText(getActivity(), R.string.registration_warning_not_valid_phone_number, Toast.LENGTH_LONG).show();
    }




    private void setUserCountry() {
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String countryShortcut =  telephonyManager.getSimCountryIso().toUpperCase();
        Locale local = new Locale("",countryShortcut);
        String countryNameString = local.getDisplayCountry();
        selectedCountry = new Country(countryShortcut,countryNameString,String.valueOf(phoneUtil.getCountryCodeForRegion(countryShortcut.toUpperCase())));
        displayCounterInfo(selectedCountry);
    }


    private void displayCounterInfo(Country country){
        countryName.setText(country.getName());
        countryCodeInput.setText(String.valueOf(phoneUtil.getCountryCodeForRegion(country.getShortCut().toUpperCase())));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == com.cnocompany.reminder.registration.CountryPicker.PICK_COUNTRY_REQ){
            if (resultCode == Activity.RESULT_OK){
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    selectedCountry = bundle.getParcelable(com.cnocompany.reminder.registration.CountryPicker.SELECTED_COUNTRY);
                    displayCounterInfo(selectedCountry);
                }
            }
        }
    }
}
