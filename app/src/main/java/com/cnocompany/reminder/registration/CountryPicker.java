package com.cnocompany.reminder.registration;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Xml;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.cnocompany.reminder.helper.Country;
import com.cnocompany.reminder.list_adapter.CountryListAdapter;
import com.cnocompany.reminder.R;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CountryPicker extends AppCompatActivity implements Comparator<Country> {
    public static final String LOG_TAG = "RP_CountryPikcer";
    private static final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    public static final int    PICK_COUNTRY_REQ = 1;
    public static final String SELECTED_COUNTRY  = "selected_country";
    private XmlPullParser parser = Xml.newPullParser();

    private List<Country> allCountriesList;
    private ArrayList<Country> searchedCounters ;

    private CountryListAdapter countryListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_picker_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        ListView countryListView = (ListView) findViewById(R.id.country_picker_listview);
        EditText countrySearch = (EditText) findViewById(R.id.country_picker_search);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        searchedCounters    = new ArrayList<>();
        //allCountriesList    = getAllCountries();


        try {
            InputStream in_s = getApplicationContext().getAssets().open("temp.xml");
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);
            allCountriesList = parseXML();
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }


        countryListAdapter = new CountryListAdapter(this,searchedCounters);
        countryListView.setAdapter(countryListAdapter);


        countryListView.setOnItemClickListener(onItemClickListener);
        countrySearch.addTextChangedListener(textWatcher);

    }

    private ArrayList<Country> parseXML() throws XmlPullParserException,IOException
    {
        ArrayList<Country> countries = null;
        int eventType = parser.getEventType();

        Country currentCountry = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    countries = new ArrayList();
                    break;


                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("country")){
                        currentCountry = new Country();
                        currentCountry.setCode(parser.getAttributeValue(null,"phoneCode"));
                        currentCountry.setName(parser.getAttributeValue(null, "name"));
                        currentCountry.setShortCut(parser.getAttributeValue(null,"code"));
                    }

                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("country") && currentCountry != null){
                        countries.add(currentCountry);
                        searchedCounters.add(currentCountry);
                    }
            }
            eventType = parser.next();
        }
        return countries;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int compare(Country country1, Country country2) {
        return country1.getName().compareTo(country2.getName());
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {


        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent result = new Intent();
            result.putExtra(SELECTED_COUNTRY,(Country) adapterView.getItemAtPosition(i));
            setResult(RESULT_OK, result);
            finish();
        }
    };





    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void afterTextChanged(Editable editable) {
            searchedCounters.clear();
            for (Country country : allCountriesList) {
                if (country.getName().toLowerCase().contains(editable.toString().toLowerCase())) {
                    searchedCounters.add(country);
                }
            }
            countryListAdapter.notifyDataSetChanged();
        }
    };
}




