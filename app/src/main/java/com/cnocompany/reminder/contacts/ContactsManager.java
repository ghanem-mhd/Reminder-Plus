package com.cnocompany.reminder.contacts;


import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;


import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.ContactDetail;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ContactsManager {

    private static final String LOG_TAG = "ContactsManager";

   /* @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static ArrayList<ContactObject> getDeviceContactsOptimized(Context context) {
        ArrayList<ContactObject> contactObjects = new ArrayList<>();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryISO =  telephonyManager.getSimCountryIso().toUpperCase();
        //HashMap<String,ContactObject> contactMap = new HashMap<>();
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER};
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, null);
        if(cursor.moveToFirst()) {
            do {

                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

                if
                ContactObject contact = new ContactObject(name);
                contact.setCountryISO(countryISO);//defult for all
                if (contact.addPhoneNumber(number)) { // return true if the phone number is mobile and return false otherwise or if there is pars error
                    contactObjects.add(contact);
                    Log.d(LOG_TAG,number);
                }

*//*                if (number != null && name != null){
                    if (contactMap.containsKey(name)){
                        ContactObject contact = contactMap.get(name);
                        contact.addPhoneNumber(number);
                    }else {
                        ContactObject contact = new ContactObject(name);
                        contact.setCountryISO(countryISO);//defult for all
                        contact.addPhoneNumber(number);
                        contactMap.put(name,contact);
                    }
                }*//*
            } while (cursor.moveToNext());
        }
        cursor.close();

       *//* for (Map.Entry<String,ContactObject> stringContactObjectEntry:contactMap.entrySet()){
            contactObjects.add(stringContactObjectEntry.getValue());
        }*//*
        return contactObjects;
    }*/

    public static boolean addContactToContactsProvider(Context context,ContactObject contactObject) {
      /*  ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ContentProviderOperation.Builder op = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, AccountConstants.ACCOUNT_NAME)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, AccountConstants.ACCOUNT_TYPE)
                .withValue(ContactsContract.RawContacts.SYNC1, contactObject.getBackendID());
        ops.add(op.build());


        op = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contactObject.getLocalName());
        ops.add(op.build());


        for(PhoneNumber phoneNumber:contactObject.getPhoneNumbers()){
            op = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.CommonDataKinds.Phone.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER, ""+phoneNumber.getPhoneNumber().getCountryCode()+phoneNumber.getPhoneNumber().getNationalNumber());
            ops.add(op.build());
        }

        op = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, Constant.MEME_TYPE)
                .withValue(ContactsContract.Data.DATA1, contactObject.getRemoteName())
                .withValue(ContactsContract.Data.DATA2, contactObject.getPhotoPath().toString());
        if (contactObject.getPhotoPath() != null){
            op.withValue(ContactsContract.Data.DATA3, contactObject.getBackendID());
        }
        ops.add(op.build());



        op.withYieldAllowed(true);
        ops.add(op.build());
        try {
             context.getContentResolver().applyBatch(ContactsContract.AUTHORITY,ops);


            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return true;
        } catch (OperationApplicationException e) {
            e.printStackTrace();
            return true;
        }*/

        return false;
    }

    public static void addContact(Context context,ContactObject contactObject){
        ContentResolver contentResolver = context.getContentResolver();

        ContentValues contentValues = new ContentValues();
        contentValues.put(ProviderContract.ContactContract.PHONE_STR,contactObject.getPhoneNumber());
        contentValues.put(ProviderContract.ContactContract.LOCAL_NAME_STR, contactObject.getLocalName());
        if (contactObject.isUsingReminderPlus())
        {
            contentValues.put(ProviderContract.ContactContract.BACKEND_USER_ID,contactObject.getBackendID());
            contentValues.put(ProviderContract.ContactContract.PHOTO_PATH,contactObject.getPhotoPath().toString());
            contentValues.put(ProviderContract.ContactContract.RP_NAME_STR,contactObject.getRemoteName());
            contentValues.put(ProviderContract.ContactContract.RP_USER_BOOL, 1);
        }else
        {
            contentValues.put(ProviderContract.ContactContract.RP_USER_BOOL, 0);
        }
        //all now contacts are not trusted...
        contentValues.put(ProviderContract.ContactContract.TRUSTED_CONTACT_BOOL,0);
        contentResolver.insert(ProviderContract.ContactContract.CONTENT_URI, contentValues);


    }

    public static boolean editPhoto(Context context,String phoneNumber,Bitmap newPhoto){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newPhoto.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        int id = -1;
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                                            new String[] { ContactsContract.CommonDataKinds.Phone.CONTACT_ID},
                                                            ContactsContract.CommonDataKinds.Phone.NUMBER + "= ?", new String[] {phoneNumber}, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        if (id != -1) {


            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            ContentProviderOperation.Builder op = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                                                    .withSelection(ContactsContract.Data.CONTACT_ID + " = ?"+ " AND " + ContactsContract.Data.MIMETYPE + "=?", new String[]{String.valueOf(id),ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE})
                                                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, baos.toByteArray());
            ops.add(op.build());
            op.withYieldAllowed(true);
            ops.add(op.build());
            try {
                context.getContentResolver().applyBatch(ContactsContract.AUTHORITY,ops);
                return true;
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            } catch (OperationApplicationException e) {
                e.printStackTrace();
                return false;
            }
        }

        return false;
    }

     public static ArrayList<ContactDetail> getFilteredContacts5(Context context,CharSequence charSequence){
        ArrayList<ContactDetail> contactDetails = new ArrayList<>();

         Log.d(LOG_TAG,charSequence.toString());

        String selection = "("+ProviderContract.ContactContract.LOCAL_NAME_STR + " LIKE ? OR " + ProviderContract.ContactContract.PHONE_STR + " LIKE ?)";
        String[] selectionArgs = new String[]{"%" + charSequence + "%", "%" + charSequence + "%"};
        String sortOrder = ProviderContract.ContactContract.LOCAL_NAME_STR + " ASC ";

       Cursor cursor = context.getContentResolver().query(ProviderContract.ContactContract.CONTENT_URI,null,selection,selectionArgs,sortOrder);

        while (cursor.moveToNext()){
            contactDetails.add(
                    new ContactDetail(
                    cursor.getLong(cursor.getColumnIndex(ProviderContract.ContactContract._ID)),
                    cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)),
                    cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHONE_STR)),
                    Uri.parse(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHOTO_PATH))),
                            cursor.getLong(cursor.getColumnIndex(ProviderContract.ContactContract.BACKEND_USER_ID))
                    ));
        }
         cursor.close();
        return contactDetails;
    }

    private static Uri makeDirForUserPhoto(Bitmap UserImage,int userID) throws IOException{
        File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File RPDir = new File(externalStoragePublicDirectory+File.separator + "Reminder Plus");
        RPDir.mkdir();
        File imageFile = new File(RPDir,"RP_user_photo_"+userID+".jpg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        UserImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        new FileOutputStream(imageFile).write(bytes.toByteArray());
        return Uri.fromFile(imageFile);
    }

    @Nullable
    public static ContactObject getContactByBackendID(Context context,long id){
        String selection = ProviderContract.ContactContract.BACKEND_USER_ID + " = ? ";
        String [] selectionARGs = new String [] {id+""};
        Cursor cursor = context.getContentResolver().query(ProviderContract.ContactContract.CONTENT_URI,null,selection,selectionARGs,null);
        ContactObject contactObject = new ContactObject();
        if(cursor.moveToFirst())
        {
            contactObject.setBackendID(id);
            contactObject.setLocalName(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)));
            contactObject.setPhotoPath(Uri.parse(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHOTO_PATH))));
            contactObject.setId(cursor.getLong(cursor.getColumnIndex(ProviderContract.ContactContract._ID)));
            cursor.close();
            return contactObject;
        }else{
            cursor.close();
            return null;
        }
    }


    /*public static ArrayList<Recipient> getFilteredContacts(Context context,CharSequence constraint){
        ArrayList<Recipient> recipients = new ArrayList<>();

        String selection = ProviderContract.ContactContract.PHONE_STR + " LIKE ? OR " +
                ProviderContract.ContactContract.LOCAL_NAME_STR + " LIKE ? OR " + ProviderContract.ContactContract.BACKEND_USER_ID + " LIKE ?";
        String[] selectionArgs = new String[]{"%" + constraint + "%", "%" + constraint + "%","%" + constraint + "%"};
        String sortOrder = ProviderContract.ContactContract.LOCAL_NAME_STR + " COLLATE LOCALIZED ASC";
        Cursor cursor = context.getContentResolver().query(ProviderContract.ContactContract.CONTENT_URI,null,selection,selectionArgs,sortOrder);

        if (cursor.moveToFirst()){
            do {

                Recipient recipient = new Recipient();
                recipient.setName(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)));
                recipient.setPhone(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHONE_STR)));
                recipient.setLookupKey(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHOTO_PATH)));
                recipient.set_ID(cursor.getInt(cursor.getColumnIndex(ProviderContract.ContactContract._ID)));
                recipients.add(recipient);
            }while (cursor.moveToNext());
        }

        return recipients;
    }*/

}
