package com.cnocompany.reminder.contacts;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.cnocompany.reminder.BuildConfig;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;

public class ContactsSync extends AsyncTask<Context,Void,ArrayList<ContactObject>>{
    private static final String LOG_TAG = "ContactsSync";
    private OnContactsCheckFinish onContactsLoaderFinish;
    private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    public ContactsSync(OnContactsCheckFinish loaderFinish) {
        super();
        this.onContactsLoaderFinish = loaderFinish;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected ArrayList<ContactObject> doInBackground(Context... contexts) {
        Context context = contexts[0];
        ArrayList<ContactObject> contactObjects = new ArrayList<>();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryISO =  telephonyManager.getSimCountryIso().toUpperCase();
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER};
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, null);
        if(cursor.moveToFirst()) {
            do {

                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));


                ContactObject contact = new ContactObject(name);
                contact.setCountryISO(countryISO);//defult for all

                if (isValidMobileNumber(number, countryISO)){
                    String selection = ProviderContract.ContactContract.PHONE_STR + " = ?";
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(ProviderContract.ContactContract.LOCAL_NAME_STR, name);
                    int numberOfContactsUpdated = context.getContentResolver().update(ProviderContract.ContactContract.CONTENT_URI,contentValues,selection,new String[]{number});
                    if (numberOfContactsUpdated == 0){ //this mean we have a contacts with ths same phone number
                        if (BuildConfig.DEBUG)
                            Log.d(LOG_TAG,"We have new Contact -> "+name);
                        contact.setPhoneNumber(number);
                        contactObjects.add(contact);
                        }
                    else {
                        if (BuildConfig.DEBUG)
                            Log.d(LOG_TAG,"We got Old Contact -_- -> "+name);
                    }
                    }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return contactObjects;
    }

    @Override
    protected void onPostExecute(ArrayList<ContactObject> contactObjects) {
        onContactsLoaderFinish.onFinish(contactObjects);
    }


    public interface OnContactsCheckFinish {
        void onFinish(ArrayList<ContactObject> newContacts);
    }


    public boolean isValidMobileNumber(String number,String countryISO) {
        // return true if the phone number is mobile and return false otherwise or if there is pars error
        Phonenumber.PhoneNumber phoneNumber ;
        try {
            phoneNumber = phoneNumberUtil.parse(number, countryISO);
            if (phoneNumberUtil.getNumberType(phoneNumber) == PhoneNumberUtil.PhoneNumberType.MOBILE) {
                return true;
            }
        } catch (NumberParseException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
