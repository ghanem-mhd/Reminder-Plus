package com.cnocompany.reminder.contacts;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.i18n.phonenumbers.PhoneNumberUtil;

public class ContactObject implements Parcelable{
    private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    private long id;
    private Uri photoPath;
    private long backendID;
    private String phoneNumber;

    public String remoteName;
    private String localName;
    private String countryISO = "SY";
    private boolean isTrusted = false;
    private boolean isUsingReminderPlus = false;

    public ContactObject() {

    }

    public ContactObject(long id, Uri photoPath, long backendID) {
        this.id = id;
        this.photoPath = photoPath;
        this.backendID = backendID;
    }

    public String getRemoteName() {
        return remoteName;
    }

    public void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBackendID() {
        return backendID;
    }

    public void setBackendID(long backendID) {
        this.backendID = backendID;
    }

    public Uri getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(Uri photoPath) {
        this.photoPath = photoPath;
    }


    @Override
    public String toString() {
        return "ContactObject{" +
                "id=" + id +
                ", photoPath=" + photoPath +
                ", backendID=" + backendID +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", remoteName='" + remoteName + '\'' +
                ", localName='" + localName + '\'' +
                ", countryISO='" + countryISO + '\'' +
                ", isTrusted=" + isTrusted +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ContactObject(String localName) {
        this.localName = localName;
    }

    public ContactObject(Parcel in){
        this.id             = in.readLong();
        this.photoPath      = Uri.parse(in.readString());
        this.backendID      = in.readLong();
        this.phoneNumber    = in.readString();
        this.localName      = in.readString();
        this.isTrusted      = in.readInt()==1 ? true: false;
        this.isUsingReminderPlus = in.readInt()==1 ? true: false;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(photoPath.toString());
        dest.writeLong(backendID);
        dest.writeString(phoneNumber);
        dest.writeString(localName);
        dest.writeInt(isTrusted ? 1 : 0);
        dest.writeInt(isUsingReminderPlus?1:0);
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {


        public ContactObject createFromParcel(Parcel in) {
            return new ContactObject(in);
        }

        public ContactObject[] newArray(int size) {
            return new ContactObject[size];
        }
    };


    public boolean isTrusted() {
        return isTrusted;
    }

    public void setIsTrusted(boolean isTrusted) {
        this.isTrusted = isTrusted;
    }

    public String getCountryISO() {
        return countryISO;
    }

    public void setCountryISO(String countryISO) {
        this.countryISO = countryISO;
    }


    public static PhoneNumberUtil getPhoneNumberUtil() {
        return phoneNumberUtil;
    }

    public static void setPhoneNumberUtil(PhoneNumberUtil phoneNumberUtil) {
        ContactObject.phoneNumberUtil = phoneNumberUtil;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    @Override
    public boolean equals(Object o) {
        ContactObject contactObject = (ContactObject) o;
        return this.localName.equals(contactObject.localName);
    }

    public boolean isUsingReminderPlus() {
        return isUsingReminderPlus;
    }

    public void setIsUsingReminderPlus(boolean isUsingReminderPlus) {
        this.isUsingReminderPlus = isUsingReminderPlus;
    }
}
