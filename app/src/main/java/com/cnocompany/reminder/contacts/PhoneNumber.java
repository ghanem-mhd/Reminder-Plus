package com.cnocompany.reminder.contacts;

import com.google.i18n.phonenumbers.Phonenumber;

import java.io.Serializable;

public class PhoneNumber implements Serializable{

    private Phonenumber.PhoneNumber phoneNumber;
    private boolean usingReminderPlus;

    public PhoneNumber() {
    }

    public PhoneNumber(Phonenumber.PhoneNumber phoneNumber, boolean usingReminderPlus) {
        this.phoneNumber = phoneNumber;
        this.usingReminderPlus = usingReminderPlus;
    }


    public Phonenumber.PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Phonenumber.PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isUsingReminderPlus() {
        return usingReminderPlus;
    }

    public void setUsingReminderPlus(boolean usingReminderPlus) {
        this.usingReminderPlus = usingReminderPlus;
    }

    @Override
    public boolean equals(Object o) {
        PhoneNumber phoneNumber = (PhoneNumber)o;
        return this.phoneNumber.equals(phoneNumber);
    }


    @Override
    public String toString() {
        return "PhoneNumber{" +
                "phoneNumber=" + phoneNumber +
                ", usingReminderPlus=" + usingReminderPlus +
                '}';
    }
}
