package com.cnocompany.reminder.alarms_utilises;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;


public class TimeAlarm implements Parcelable{

    private long id;
    private String time;
    private Uri alarmTone;
    private boolean isSilent;
    private String title;
    private String description;
    private long creatorID;

    public TimeAlarm() {

    }


    public Uri getAlarmTone() {
        return alarmTone;
    }

    public void setAlarmTone(Uri alarmTone) {
        this.alarmTone = alarmTone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String message) {
        this.description = message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(long creatorID) {
        this.creatorID = creatorID;
    }

    public boolean isSilent() {
        return isSilent;
    }

    public void setIsSilent(boolean isSilent) {
        this.isSilent = isSilent;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeLong(creatorID);
        parcel.writeString(time);
        parcel.writeInt(isSilent?1:0);
    }

    public TimeAlarm(Parcel in){
        this.id             = in.readLong();
        this.title          = in.readString();
        this.description    = in.readString();
        this.creatorID      = in.readLong();
        this.time           = in.readString();
        this.isSilent       = in.readInt() == 1 ? true: false;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public TimeAlarm createFromParcel(Parcel in) {
            return new TimeAlarm(in);
        }

        public TimeAlarm[] newArray(int size) {
            return new TimeAlarm[size];
        }
    };


    @Override
    public String toString() {
        return "TimeAlarm{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", alarmTone=" + alarmTone +
                ", isSilent=" + isSilent +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", creatorID=" + creatorID +
                '}';
    }
}