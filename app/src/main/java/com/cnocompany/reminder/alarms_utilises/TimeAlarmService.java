package com.cnocompany.reminder.alarms_utilises;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class TimeAlarmService extends Service {

    public static String TAG = TimeAlarmService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Intent alarmIntent = new Intent(getApplicationContext(), AlarmActivity.class);
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            alarmIntent.putExtras(intent);
            getApplication().startActivity(alarmIntent);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }





}