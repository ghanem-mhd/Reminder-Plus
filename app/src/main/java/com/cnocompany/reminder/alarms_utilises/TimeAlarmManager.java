package com.cnocompany.reminder.alarms_utilises;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.cnocompany.reminder.BuildConfig;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.date_objects.Reminder;

import java.util.ArrayList;
import java.util.Date;


public class TimeAlarmManager extends BroadcastReceiver {


    private static final String TAG = "alarmManager";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

        }
    }


    public static ArrayList<Reminder> getAllReminderWithAlarm(Context context){
        String selection = ProviderContract.ReminderContract.HAS_ALARM_BOOL + " = ? AND "+ ProviderContract.ReminderContract.TYPE_INT + " = ?";
        String selectionARG [] = new String[]{"1","2"};
        ArrayList<Reminder> reminders = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(ProviderContract.ReminderContract.CONTENT_URI,null,selection,selectionARG,null);
        if(cursor.moveToFirst()){
            do {
                reminders.add(Reminder.getReminderObjectFromCursor(cursor));
            }while (cursor.moveToNext());
        }
        cursor.close();
        return reminders;
    }

    public static void setSnoozeToReminder(Context context,Reminder reminder,Long milliSecondDelay){
        try {
            Date alarmDate  = null;
            if (reminder.getType() == 1){
                alarmDate = Constant.DB_formatter.parse(reminder.getTime());
            }else{
                alarmDate = new Date();
            }
            PendingIntent pendingIntent = createPendingIntentForAlarm(context, reminder.get_ID());
            android.app.AlarmManager alarmManager = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(android.app.AlarmManager.RTC_WAKEUP, alarmDate.getTime()+milliSecondDelay, pendingIntent);
            } else {
                alarmManager.set(android.app.AlarmManager.RTC_WAKEUP, alarmDate.getTime()+milliSecondDelay, pendingIntent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void setTimeAlarm(Context context, Reminder reminder) {
        try {
            Date alarmDate = Constant.DB_formatter.parse(reminder.getTime());
            if (alarmDate.getTime() > new Date().getTime()){
                PendingIntent pendingIntent = createPendingIntentForAlarm(context, reminder.get_ID());
                android.app.AlarmManager alarmManager = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(android.app.AlarmManager.RTC_WAKEUP, alarmDate.getTime(), pendingIntent);
                } else {
                    alarmManager.set(android.app.AlarmManager.RTC_WAKEUP, alarmDate.getTime(), pendingIntent);
                }
            }
            else{
                if (BuildConfig.DEBUG){
                    Log.d(TAG, reminder +" is in past.");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }




    public static void cancelAlarms(Context context) {
        android.app.AlarmManager alarmManager = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Cursor cursor = context.getContentResolver().query(ProviderContract.AlarmContract.CONTENT_URI,null,null,null,null);
        if(cursor.moveToFirst()){do{
            long alarm_id =  cursor.getLong(cursor.getColumnIndex(ProviderContract.AlarmContract._ID));
            Intent intent = new Intent(context, TimeAlarmService.class);
            PendingIntent pIntent = PendingIntent.getService(context, (int) alarm_id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pIntent);
            }while (cursor.moveToFirst());
        }
        cursor.close();
    }

    public static void setAllAlarms(Context context,ArrayList<Reminder> reminders){
        for (Reminder reminder : reminders) {
            setTimeAlarm(context, reminder);
        }
    }

    private static PendingIntent createPendingIntentForAlarm(Context context,long reminder_ID) {
        Intent intent = new Intent(context.getApplicationContext(), TimeAlarmService.class);
        intent.putExtra(AlarmActivity.ALARM_ARG, reminder_ID);
        return PendingIntent.getService(context.getApplicationContext(), (int) reminder_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    //to delete an alarm from alarm manager the only way is to reset all alarms without the one which we want to delete
    public static void removeReminderAlarm(Context context,long reminderID){
        String selection = ProviderContract.ReminderContract._ID +" = ?";
        String [] selectionARGS = new String[]{reminderID+""};
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProviderContract.ReminderContract.HAS_ALARM_BOOL, 0);
        int itemUpdatedCount = context.getContentResolver().update(ProviderContract.ReminderContract.CONTENT_URI,contentValues,selection,selectionARGS);
        //todo one unnecessary query to db
        //this mean we find an alarm to this reminder
        if (itemUpdatedCount > 0){
            //this will reset all alarms
            setAllAlarms(context,getAllReminderWithAlarm(context));
        }
    }

    public static void makeAlarmSilent(Context context,long reminderID,int isSilent){
        String selection = ProviderContract.ReminderContract._ID +" = ?";
        String [] selectionARGS = new String[]{reminderID+""};
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProviderContract.ReminderContract.SILENT_ALARM_BOOL,isSilent);
        int itemUpdatedCount = context.getContentResolver().update(ProviderContract.ReminderContract.CONTENT_URI,contentValues,selection,selectionARGS);
        //this mean we find an alarm to this reminder
        if (itemUpdatedCount > 0){
            //this will reset all alarms
            //cancelAlarms(context);
            setAllAlarms(context,getAllReminderWithAlarm(context));
        }
    }


    public static void markAlarmAsFinish(Context context,long reminderID){
        String selection = ProviderContract.ReminderContract._ID +" = ?";
        String [] selectionARGS = new String[]{reminderID+""};
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProviderContract.ReminderContract.HAS_ALARM_BOOL, 0);
        context.getContentResolver().update(ProviderContract.ReminderContract.CONTENT_URI,contentValues,selection,selectionARGS);
    }

}