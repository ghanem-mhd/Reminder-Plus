package com.cnocompany.reminder.alarms_utilises;


import android.app.Activity;

import android.content.Context;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;

import android.view.KeyEvent;
import android.view.WindowManager;

import android.widget.TextView;


import com.cnocompany.reminder.R;
import com.cnocompany.reminder.date_objects.Reminder;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AlarmActivity extends Activity {
    public static final String ALARM_ARG = "alarmARG";
    private static final String TAG = "AlarmActivity";

    private Long reminder_ID = -1l;
    private Reminder firedReminder = null;

    private MediaPlayer mPlayer;

    private Vibrator vibrator = null;

    @Bind(R.id.alarm_screen_title ) TextView title;
    @Bind(R.id.alarm_screen_description ) TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_screen_activity);
        setFinishOnTouchOutside(false);
        ButterKnife.bind(this);

        if (savedInstanceState == null){
            if (getIntent() != null) {
                reminder_ID = getIntent().getLongExtra(ALARM_ARG,-1);
                firedReminder = Reminder.getReminderByID(this.getApplicationContext(),reminder_ID);
                if (firedReminder == null)
                    finish();
                TimeAlarmManager.markAlarmAsFinish(this,reminder_ID);
            }else  finish();


            makeAlarmScreen();

            setUpVibrator();



            if (firedReminder.getIsSilent() == 1) {
                //todo change 20 sec to 60 sec
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                    }
                }, 20000);
            }else{
                setUpSoundPlayer();
            }
        }
    }

    private void setUpSoundPlayer() {
        mPlayer = new MediaPlayer();
        try {
            Uri defaultRingtoneUrineUri = RingtoneManager.getActualDefaultRingtoneUri(this.getApplicationContext(), RingtoneManager.TYPE_ALARM);
            mPlayer.setDataSource(this, defaultRingtoneUrineUri);
            mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mPlayer.setLooping(true);
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.start();
    }

    private void setUpVibrator() {
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 100 milliseconds
        // Sleep for 1000 milliseconds
        long[] pattern = {0, 1000, 1000};
        vibrator.vibrate(pattern, 0);
    }

    private void makeAlarmScreen() {
        title.setText(firedReminder.getTitle());
        content.setText(String.format("%s - %s.", firedReminder.getCreatorName().trim(), firedReminder.getDescription().trim()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Set the window to keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

    }

    @Override
    protected void onStop() {
        super.onStop();
        ButterKnife.unbind(this);
        releaseAllServices();
        finish();
    }

    @OnClick(R.id.alarm_screen_done_button)
    public void done(){
        finish();
    }

    @OnClick(R.id.alarm_screen_snooze_button)
    public void snooze(){
        TimeAlarmManager.setSnoozeToReminder(AlarmActivity.this, firedReminder,20000l);
        AlarmActivity.this.finish();
    }

    private void releaseAllServices() {
        if (firedReminder.getIsSilent() == 1)
            mPlayer.stop();
        vibrator.cancel();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_MUTE) {
            if (firedReminder.getIsSilent() == 1)
                mPlayer.stop();
            return true;
        }
        return false;
    }
}
