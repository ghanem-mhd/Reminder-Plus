package com.cnocompany.reminder.authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.cnocompany.reminder.R;
import com.cnocompany.reminder.registration.RegistrationActivity;


public class AccountAuthenticator extends AbstractAccountAuthenticator{
    private final static String LOG_TAG = "RP_AccountAuth";
    private final Context mContext;
    private final Handler handler = new Handler();

    public AccountAuthenticator(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse accountAuthenticatorResponse, String s) {
        return null;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {

        final Bundle bundle = new Bundle();
        AccountManager am = AccountManager.get(mContext);
        Log.d(LOG_TAG,accountType+" "+authTokenType);

        //get all account and check if there is reminder plus account.
        Account accounts []  = am.getAccounts();
        for(Account account:accounts){
            if (account.type.equals(AccountConstants.ACCOUNT_TYPE) && authTokenType == null) {
                //return error and show toast.
                bundle.putInt(AccountManager.KEY_ERROR_CODE, 1);
                bundle.putString(AccountManager.KEY_ERROR_MESSAGE, "multi individual account error" );
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, R.string.account_auth_warning_multi_individual_account, Toast.LENGTH_LONG).show();
                    }
                });
                return bundle;
            }
        }

        if (authTokenType == null || authTokenType.equals(AccountConstants.AUTH_TOKEN_IND)){
            final Intent intent = new Intent(mContext, RegistrationActivity.class);
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
            bundle.putParcelable(AccountManager.KEY_INTENT, intent);
            return bundle;
        }

        if (authTokenType.equals(AccountConstants.AUTH_TOKEN_ORG)) {
            final Intent intent = new Intent(mContext, organizationAuthenticator.class);
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
            bundle.putParcelable(AccountManager.KEY_INTENT, intent);
            return bundle;
        }


        return null;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, Bundle bundle) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        final AccountManager accountManager = AccountManager.get(mContext);
        String authToken = accountManager.peekAuthToken(account, authTokenType);

        if (TextUtils.isEmpty(authToken)) {
            if (TextUtils.equals(authTokenType,AccountConstants.AUTH_TOKEN_IND)){
                Log.d(LOG_TAG,"request individual token from server");
            }

            if (TextUtils.equals(authTokenType,AccountConstants.AUTH_TOKEN_ORG)){
                Log.d(LOG_TAG,"request organization token from server");
            }
        }

        // If we get an authToken - we return it
        if (!TextUtils.isEmpty(authToken)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
            return result;
        }

        return null;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String s, Bundle bundle) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String[] strings) throws NetworkErrorException {
        return null;
    }



}
