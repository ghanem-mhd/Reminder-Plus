package com.cnocompany.reminder.authenticator;


import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.Button;

import com.cnocompany.reminder.R;

import com.rey.material.widget.EditText;

public class organizationAuthenticator extends AccountAuthenticatorActivity implements View.OnClickListener{

    public static final String LOG_TAG = "RP_org_auth";

    private AccountManager accountManager;

    private EditText usernameInput;
    private EditText passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);



        accountManager = AccountManager.get(getBaseContext());




        usernameInput = (EditText) findViewById(R.id.login_username_input);
        passwordInput = (EditText) findViewById(R.id.login_password_input);
        Button loginButton = (Button) findViewById(R.id.login_button);


        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:

                submit();
                break;
        }
    }

    private void submit() {
        String username = usernameInput.getText().toString();
        String password = passwordInput.getText().toString();
        if (username.length()==0)
            Toast.makeText(this,R.string.login_warning_no_username,Toast.LENGTH_LONG);
        else
            if (password.length()==0)
                Toast.makeText(this,R.string.login_warning_not_password,Toast.LENGTH_LONG);
            else {
                String authToken    = "dfa3";

                Account account = new Account(username, AccountConstants.ACCOUNT_TYPE);
                accountManager.addAccountExplicitly(account, password, null);
                accountManager.setAuthToken(account, AccountConstants.AUTH_TOKEN_IND, authToken);

                Bundle data = new Bundle();
                data.putString(AccountManager.KEY_ACCOUNT_NAME, username);
                data.putString(AccountManager.KEY_ACCOUNT_TYPE, AccountConstants.ACCOUNT_TYPE);
                data.putString(AccountManager.KEY_AUTHTOKEN, authToken);

                setAccountAuthenticatorResult(data);
                setResult(RESULT_OK);
                finish();
            }
    }
}
