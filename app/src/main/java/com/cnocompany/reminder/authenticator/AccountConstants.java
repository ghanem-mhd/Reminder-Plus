package com.cnocompany.reminder.authenticator;

public class AccountConstants {
    public static final String ACCOUNT_TYPE     = "com.ReminderPlus";
    public static final String ACCOUNT_NAME     = "Reminder Plus";

    public static final String AUTH_TOKEN_IND   = "individualAccount";
    public static final String AUTH_TOKEN_ORG   = "organizationAccount";



    public final static String ARG_ACCOUNT_TYPE             = "ACCOUNT_TYPE";
    public final static String ARG_ACCOUNT_NAME             = "ACCOUNT_NAME";
    public final static String ARG_AUTH_TOKEN_TYPE          = "AUTH_TYPE";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT    = "IS_ADDING_ACCOUNT";

}
