package com.cnocompany.reminder.authenticator;


import android.accounts.AccountManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AuthenticatorService extends Service{
    private final static String LOG_TAG = "RP_AuthService";
    private AccountAuthenticator authenticator;

    @Override
    public void onCreate() {
        if (authenticator == null)
            authenticator  = new AccountAuthenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
      if (intent.getAction().equals(AccountManager.ACTION_AUTHENTICATOR_INTENT))
            return authenticator.getIBinder();
        else
            return null;
    }
}
