package com.cnocompany.reminder.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.cnocompany.reminder.R;

public class DialogManager {

    public static AlertDialog ShowConfirmDialog(final Activity activity, String title, String message, String positive, String negative){
       return new AlertDialog.Builder(activity)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }

                })
                .setNegativeButton(negative, null)
                .show();
    }


    public static void ShowConfirmDialog(final Activity activity,String message,String positive,String negative){
        new AlertDialog.Builder(activity)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }

                })
                .setNegativeButton(negative, null)
                .show();
    }


    public static void ShowConfirmDialog(final Activity activity){
        new AlertDialog.Builder(activity)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setMessage("Are you sure you want to discard the change?")
                .setPositiveButton("Keep Editing", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }

                })
                .setNegativeButton("Discard", null)
                .show();
    }


    public static ProgressDialog getProgressDialog(Activity activity,String message){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(message);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setIndeterminateDrawable(ContextCompat.getDrawable(activity, R.drawable.progress));
        return dialog;
    }


    public static ProgressDialog getProgressDialog(Activity activity,String title,String message){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setIndeterminateDrawable(ContextCompat.getDrawable(activity, R.drawable.progress));
        return dialog;
    }
}
