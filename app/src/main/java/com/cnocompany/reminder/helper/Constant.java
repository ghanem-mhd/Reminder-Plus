package com.cnocompany.reminder.helper;

import java.text.SimpleDateFormat;

/**
 * Created by Mo7ammed on 5/19/2015.
 */
public final class Constant {

    public  static final SimpleDateFormat DB_formatter          = new SimpleDateFormat(Constant.DateTime_Format_IN_DB);
    public  static final SimpleDateFormat ONLY_TIME_FORMATTER   = new SimpleDateFormat(Constant.Time_Format);

    public static final String DateTime_Format_IN_DB = "yyyy-MM-dd HH:mm";
    public static final String DateOnly_Format_IN_DB = "yyyy-MM-dd";
    public static final String Date_Format = "MMM dd";
    public static final String Time_Format = "hh:mm a";
    public static final String Time_Date_Format = "yyyy/MM/dd   HH:mm";
    public static final String Day_Month_Time = "EEE, MMM dd, hh:mm a";

    public static final String MEME_TYPE = "RPContact";



    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME = "com.google.android.gms.location.sample.locationaddress";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";



}
