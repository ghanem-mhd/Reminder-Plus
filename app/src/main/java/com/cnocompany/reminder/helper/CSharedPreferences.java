package com.cnocompany.reminder.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mo7ammed on 5/14/2015.
 */
public class CSharedPreferences {
    public static final String USER_INFO = "user_info";
    public static final String UTILISES  = "utilises";
    public static final String Logged_IN  = "logged_in";
    public static final String SESSION_ID = "session_id";
    public static final String USER_PROFILE_NAME  = "user_profile_name";
    public static final String USER_PROFILE_EMAIL  = "user_email";
    public static final String SYNC_OFF = "sync_on";
    public static final String USER_ID = "_ID";


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public CSharedPreferences(String sharedPreferencesName, Context context) {
        sharedPreferences = context.getSharedPreferences(sharedPreferencesName, 0);
        editor = sharedPreferences.edit();
    }

    public String getString(String variableName, String defultValue) {
        String returnedString = sharedPreferences.getString(variableName, defultValue);

        if (returnedString != null)
            return returnedString;
        else
            return null;
    }

    public boolean getBoolean(String variableName, boolean defultValue) {
        return sharedPreferences.getBoolean(variableName, defultValue);
    }


    public boolean putString(String variableName, String variableValue) {
        editor.putString(variableName, variableValue);
        return editor.commit();
    }

    public boolean putBoolean(String variableName, boolean variableValue) {
        editor.putBoolean(variableName, variableValue);
        return editor.commit();
    }

    public void deleteShared(){
        editor.clear().commit();
    }

}
