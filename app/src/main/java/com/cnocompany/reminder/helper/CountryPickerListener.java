package com.cnocompany.reminder.helper;

public interface CountryPickerListener {
    void onSelectCountry(String name, String code);
}