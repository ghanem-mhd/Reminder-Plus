package com.cnocompany.reminder.helper;

import android.os.Parcel;
import android.os.Parcelable;

public class Country implements Parcelable{
    private String shortCut;
    private String code;
    private String name;


    public Country() {
    }

    public Country(String shortCut, String name, String code) {
        this.shortCut = shortCut;
        this.name = name;
        this.code = code;
    }

    public Country(Parcel in ){
        String[] data = new String[3];

        in.readStringArray(data);
        this.shortCut = data[0];
        this.name = data[1];
        this.code = data[2];
    }

    public String getShortCut() {
        return shortCut;
    }

    public void setShortCut(String shortCut) {
        this.shortCut = shortCut;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Country{" +
                "shortCut='" + shortCut + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{this.shortCut, this.name, this.code});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        public Country[] newArray(int size) {
            return new Country[size];
        }
    };
}