package com.cnocompany.reminder.helper;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;


public class ContactDetail implements Parcelable {
    public static final Creator<ContactDetail> CREATOR = new Creator<ContactDetail>() {
        public ContactDetail createFromParcel(Parcel source) {
            return new ContactDetail(source);
        }
        public ContactDetail[] newArray(int size) {
            return new ContactDetail[size];
        }
    };


    String name;
    String phone;
    Uri photoUri;
    Long _ID;
    long backendID;

    public ContactDetail() {

    }

    public ContactDetail(Long _ID, String name, String phone, Uri photoUri) {
        this.name = name;
        this.phone = phone;
        this._ID = _ID;
        this.photoUri = photoUri;
    }


    public ContactDetail(Long _ID, String name, String phone, Uri photoUri,long backendID) {
        this.name = name;
        this.phone = phone;
        this._ID = _ID;
        this.photoUri = photoUri;
        this.backendID = backendID;
    }

    private ContactDetail(Parcel in) {
        this.name = in.readString();
        this.phone = in.readString();
        this.photoUri = Uri.parse(in.readString());
        this.backendID = in.readLong();
        this._ID = in.readLong();
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public Long get_ID() {
        return _ID;
    }

    public Long getBackendID() {
        return backendID;
    }

    public void setBackendID(Long backendID) {
        this.backendID = backendID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.photoUri.toString());
        dest.writeLong(this.backendID);
        dest.writeLong(this._ID);
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setBackendID(long backendID) {
        this.backendID = backendID;
    }

    public void set_ID(Long _ID) {
        this._ID = _ID;
    }
}