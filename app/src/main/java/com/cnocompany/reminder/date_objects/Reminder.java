package com.cnocompany.reminder.date_objects;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.cnocompany.reminder.alarms_utilises.TimeAlarm;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.ContactDetail;
import com.cnocompany.reminder.location_utilises.LocationAlarm;

import java.io.Serializable;
import java.util.ArrayList;


public class Reminder implements Serializable{


    private long backendReminderID;
    private long backendUserReminderID;
    private long backendCreatorID;

    private long _ID;
    private String title;
    private String description;
    private String time;

    private ArrayList<Long> participantIDS; // will contain id of participant without the creator
    private String creatorPhoneNumber;  //this field used in case the reminder add be non exist contact
    private int type;
    private int isSilent;
    private double lat;
    private double lng;
    private float radius;
    private int hasAlarm;
    private int status;
    private String creatorName = null;
    private String address;


    public Reminder() {
        participantIDS = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getBackendReminderID() {
        return backendReminderID;
    }

    public void setBackendReminderID(long backendReminderID) {
        this.backendReminderID = backendReminderID;
    }

    public long getBackendUserReminderID() {
        return backendUserReminderID;
    }

    public void setBackendUserReminderID(long backendUserReminderID) {
        this.backendUserReminderID = backendUserReminderID;
    }

    public long getBackendCreatorID() {
        return backendCreatorID;
    }

    public void setBackendCreatorID(long backendCreatorID) {
        this.backendCreatorID = backendCreatorID;
    }

    public String getCreatorPhoneNumber() {
        return creatorPhoneNumber;
    }

    public void setCreatorPhoneNumber(String creatorPhoneNumber) {
        this.creatorPhoneNumber = creatorPhoneNumber;
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        if (type == 1 || type ==2)
            this.type = type;
        else
            new IllegalArgumentException("Only 1 (time reminder) or 2 (location reminder)");
    }

    public int getIsSilent() {
        return isSilent;
    }

    public void setIsSilent(int isSilent) {
        if (isSilent == 1 || isSilent == 0)
            this.isSilent = isSilent;
        else
            new IllegalArgumentException("Only 1 (true) or 0 (false)");
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public int getHasAlarm() {
        return hasAlarm;
    }

    public ArrayList<Long> getParticipantIDS() {
        return participantIDS;
    }

    public void setParticipantIDS(ArrayList<Long> participantIDS) {
        this.participantIDS = participantIDS;
    }

    public void setParticipantIDS(Long...ids){
        for (Long id : ids) {
            participantIDS.add(id);
        }
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Nullable
    public static Reminder getReminderObjectFromCursor(Cursor cursor){
        Reminder reminder = new Reminder();
        try {
            reminder.set_ID(cursor.getLong(cursor.getColumnIndex(ProviderContract.ReminderContract._ID)));
            reminder.setTitle(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.TITLE)));
            reminder.setDescription(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.DESCRIPTION)));
            reminder.setBackendCreatorID(cursor.getInt(cursor.getColumnIndex(ProviderContract.ReminderContract.BACKEND_CREATOR_ID)));
            reminder.setBackendReminderID(cursor.getInt(cursor.getColumnIndex(ProviderContract.ReminderContract.BACKEND_REMINDER_ID)));
            reminder.setBackendUserReminderID(cursor.getInt(cursor.getColumnIndex(ProviderContract.ReminderContract.BACKEND_USER_REMINDER_ID)));
            reminder.setIsSilent(cursor.getInt(cursor.getColumnIndex(ProviderContract.ReminderContract.SILENT_ALARM_BOOL)));
            reminder.setStatus(cursor.getInt(cursor.getColumnIndex(ProviderContract.ReminderContract.STATUS)));

            int creatorNameColumnIndex = cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR);
            if (creatorNameColumnIndex != -1)
                reminder.setCreatorName(cursor.getString(creatorNameColumnIndex));


            int reminderType = cursor.getInt(cursor.getColumnIndex(ProviderContract.ReminderContract.TYPE_INT));
            if (reminderType == ProviderContract.ReminderContract.TIME_REMINDER) {
                reminder.setType(reminderType);
                reminder.setTime(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.TIME)));
            }else{
                reminder.setType(reminderType);
                reminder.setLat(Double.parseDouble(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.LAT))));
                reminder.setLng(Double.parseDouble(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.LNG))));
                reminder.setRadius(Float.parseFloat(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.RADIUS))));
                reminder.setAddress(cursor.getString(cursor.getColumnIndex(ProviderContract.ReminderContract.ADDRESS)));
            }

            return reminder;
        }catch (RuntimeException e){
            e.printStackTrace();
            return null;
        }

    }

    public static long AddReminderToContentProvider(Context context,Reminder reminder){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ProviderContract.ReminderContract.TITLE,reminder.getTitle());
        contentValues.put(ProviderContract.ReminderContract.DESCRIPTION, reminder.getDescription());
        contentValues.put(ProviderContract.ReminderContract.HAS_ALARM_BOOL, 1);     //all new reminder has an alarm
        contentValues.put(ProviderContract.ReminderContract.TYPE_INT, reminder.getType());
        contentValues.put(ProviderContract.ReminderContract.BACKEND_CREATOR_ID, reminder.getBackendCreatorID());
        contentValues.put(ProviderContract.ReminderContract.BACKEND_REMINDER_ID, reminder.getBackendReminderID());
        contentValues.put(ProviderContract.ReminderContract.BACKEND_USER_REMINDER_ID, reminder.getBackendUserReminderID());
        contentValues.put(ProviderContract.ReminderContract.STATUS,reminder.getStatus());

        if (reminder.getType() == 1){ //time reminder
            contentValues.put(ProviderContract.ReminderContract.TIME,reminder.getTime());
            contentValues.put(ProviderContract.ReminderContract.SILENT_ALARM_BOOL,reminder.getIsSilent());
        }else{ // location reminder
            contentValues.put(ProviderContract.ReminderContract.ADDRESS,reminder.getAddress());
            contentValues.put(ProviderContract.ReminderContract.LAT,reminder.getLat()+"");
            contentValues.put(ProviderContract.ReminderContract.LNG,reminder.getLng()+"");
            contentValues.put(ProviderContract.ReminderContract.RADIUS,reminder.getRadius());
        }
        Uri reminderUri = context.getContentResolver().insert(ProviderContract.ReminderContract.CONTENT_URI, contentValues);

        if (reminder.getParticipantIDS().size() > 0) {
            for (Long participantID : reminder.getParticipantIDS()) {
                contentValues = new ContentValues();
                contentValues.put(ProviderContract.ContactReminderContract.BACKEND_CONTACT_ID, participantID);
                contentValues.put(ProviderContract.ContactReminderContract.BACKEND_REMINDER_ID, reminder.getBackendReminderID());
                context.getContentResolver().insert(ProviderContract.ContactReminderContract.CONTENT_URI, contentValues);

            }

        }
        if (reminderUri != null){ //error in insert due unique constrant or any thins else
            long reminderID= Long.parseLong(reminderUri.getLastPathSegment());
            reminder.set_ID(reminderID);
            return reminderID;
        }else{
            reminder.set_ID(-2);
            return -2;
        }
    }

    public TimeAlarm getAlarm(){
        TimeAlarm timeAlarm = new TimeAlarm();
        timeAlarm.setCreatorID(getBackendCreatorID());
        timeAlarm.setDescription(getDescription());
        timeAlarm.setId(get_ID());
        timeAlarm.setTitle(getTitle());
        timeAlarm.setIsSilent(getIsSilent() == 1);
        timeAlarm.setTime(getTime());
        return timeAlarm;
    }

    public LocationAlarm getLocationAlarm(){
        LocationAlarm locationAlarm = new LocationAlarm();
        locationAlarm.setId(get_ID() + "");
        locationAlarm.setTitle(getTitle());
        locationAlarm.setDescription(getDescription());
        locationAlarm.setLatitude(getLat());
        locationAlarm.setLongitude(getLng());
        locationAlarm.setRadius(getRadius());
        return locationAlarm;

    }

    public static Reminder getReminderByID(Context context,long reminderID){
        Uri uri = ContentUris.withAppendedId(ProviderContract.ReminderContract.CONTENT_URI,reminderID);
        Cursor cursor = context.getContentResolver().query(uri,null,null,null,null);
        if (cursor.moveToFirst()){
            Reminder returnedReminder =  Reminder.getReminderObjectFromCursor(cursor);
            cursor.close();
            return returnedReminder;
        }
        cursor.close();
        return null;
    }


    public static ArrayList<ContactDetail> getParticipant(Context context,long backendReminderID){
        ArrayList<ContactDetail> contactDetails = new ArrayList<>();
        String selection = ProviderContract.ContactReminderContract.BACKEND_REMINDER_ID + " = ?";
        Cursor cursor = context.getContentResolver().query(ProviderContract.ContactReminderContract.CONTENT_URI, null, selection, new String[]{backendReminderID+ ""}, null);
        if (cursor.moveToFirst()){
            do{

                contactDetails.add(
                        new ContactDetail(
                                cursor.getLong(cursor.getColumnIndex(ProviderContract.ContactContract._ID)),
                                cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)),
                                cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHONE_STR)),
                                Uri.parse(cursor.getString(cursor.getColumnIndex(ProviderContract.ContactContract.PHOTO_PATH))),
                                cursor.getLong(cursor.getColumnIndex(ProviderContract.ContactContract.BACKEND_USER_ID))
                        ));

            }while (cursor.moveToNext());
        }
        cursor.close();

        return contactDetails;
    }
}
