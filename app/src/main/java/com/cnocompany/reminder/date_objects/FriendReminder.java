package com.cnocompany.reminder.date_objects;

/**
 * Created by Mo7ammed on 5/19/2015.
 */
public class FriendReminder {
    private long id;
    private String reminder_id;
    private String friend_id;
    public FriendReminder(String friend_id, String reminder_id) {
        this.friend_id = friend_id;
        this.reminder_id = reminder_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReminder_id() {
        return reminder_id;
    }

    public void setReminder_id(String reminder_id) {
        this.reminder_id = reminder_id;
    }

    public String getFriend_id() {
        return friend_id;
    }

    public void setFriend_id(String friend_id) {
        this.friend_id = friend_id;
    }
}
