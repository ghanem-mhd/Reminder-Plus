package com.cnocompany.reminder.date_objects;


import android.net.Uri;

public class Contact implements NewFeed{

    private long id;
    private String name;
    private String phone;
    private Uri photoUri;


    public Contact(long id, String name, String phone, Uri photoUri) {
        this.id = id;
        this.photoUri = photoUri;
        this.phone = phone;
        this.name = name;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }
}
