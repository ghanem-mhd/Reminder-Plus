package com.cnocompany.reminder.date_objects;

/**
 * Created by Mo7ammed on 6/5/2015.
 */
public class FriendshipRequest {

    private long id;
    private int form;
    private int to ;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getForm() {
        return form;
    }

    public void setForm(int form) {
        this.form = form;
    }
}
