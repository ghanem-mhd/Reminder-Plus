package com.cnocompany.reminder.location_utilises;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


import com.cnocompany.reminder.BuildConfig;
import com.cnocompany.reminder.date_objects.Reminder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

public class LocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    private static final String TAG = "LocationManager";
    public static final String REMINDER_ARG = "location_reminder_ID_ARG";
    protected GoogleApiClient mGoogleApiClient;
    private Context mContext;
    private static LocationManager instance = null;

    private ArrayList<Reminder> addedLocationAlarms = new ArrayList<>();
    private ArrayList<String> deletedLocationAlarms = new ArrayList<>(); //only need the id

    public static LocationManager getInstance(Context context) {
        if (instance == null){
            instance = new LocationManager(context);
        }
        return instance;
    }

    public static LocationManager getInstance(){
        return instance;
    }

    private LocationManager(Context context) {
        mContext = context;
        buildGoogleApiClient();
    }


    public boolean addLocationReminder(Reminder locationAlarm){
        if (!mGoogleApiClient.isConnected()) {
            addedLocationAlarms.add(locationAlarm);
            if (BuildConfig.DEBUG)
                Log.d(TAG,"add failed Not Connected  "+locationAlarm.toString());
            return false;
        }else {
            Log.d(TAG,"add done Connected  "+locationAlarm.toString());
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    // The GeofenceRequest object.
                    getGeofencingRequest(getGeofence(locationAlarm)),
                    // A pending intent that that is reused when calling removeGeofences(). This
                    // pending intent is used to generate an intent when a matched geofence
                    // transition is observed.
                    getGeofencePendingIntent(locationAlarm)
            ).setResultCallback(this); // Result processed in onResult().
            return true;
        }
    }

    public boolean removeLocationReminder(String locationAlarmID){
        if (!mGoogleApiClient.isConnected()) {
            deletedLocationAlarms.add(locationAlarmID);
            if (BuildConfig.DEBUG)
                Log.d(TAG,"remove failed Not Connected");
            return false;
        }else {
            List<String> geofancesID = new ArrayList<>();
            geofancesID.add(locationAlarmID);
            LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, geofancesID).setResultCallback(this);
            return true;
        }
    }

    public boolean removeLocationReminder(ArrayList<String> locationAlarmsIDs){
        if (!mGoogleApiClient.isConnected()) {
            deletedLocationAlarms.addAll(locationAlarmsIDs);
            if (BuildConfig.DEBUG)
                Log.d(TAG,"remove failed Not Connected");
            return false;
        }else {
            LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, locationAlarmsIDs).setResultCallback(this);
            return true;
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private GeofencingRequest getGeofencingRequest(Geofence geofence) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofence(geofence);
        // Return a GeofencingRequest.
        return builder.build();
    }

    private Geofence getGeofence(Reminder locationAlarm){
        Geofence.Builder geofanceBuilder = new Geofence.Builder();
        geofanceBuilder.setRequestId(locationAlarm.get_ID()+"");
        geofanceBuilder.setCircularRegion(locationAlarm.getLat(), locationAlarm.getLng(), locationAlarm.getRadius());
        geofanceBuilder.setExpirationDuration(Geofence.NEVER_EXPIRE);
        geofanceBuilder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER);
        return geofanceBuilder.build();
    }

    private PendingIntent getGeofencePendingIntent(Reminder locationAlarm) {
        Intent intent = new Intent(mContext, GeofenceTransitionsIntentService.class);
        intent.putExtra(REMINDER_ARG,Long.parseLong(locationAlarm.get_ID()+""));
        return PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        for (Reminder locationAlarm : addedLocationAlarms) {
            addedLocationAlarms.remove(locationAlarm);
            addLocationReminder(locationAlarm);
        }

        for (String locationAlarmID:deletedLocationAlarms){
            deletedLocationAlarms.remove(locationAlarmID);
            removeLocationReminder(locationAlarmID);
        }
    }
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }
    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason.
        Log.i(TAG, "Connection suspended");
        // onConnected() will be called again automatically when the service reconnects
    }

    @Override
    public void onResult(Status status) {
        if (status.isSuccess()) {
            Log.d(TAG,"location reminder successfully requst(Remove/Added).");
        } else {
            // Get the status code for the error and log it using a user-friendly message.
            String errorMessage = GeofenceErrorMessages.getErrorString(mContext, status.getStatusCode());
            Log.e(TAG, errorMessage);
        }
    }


    public void disConnect(){
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }
}
