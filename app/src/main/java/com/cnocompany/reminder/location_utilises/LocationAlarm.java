package com.cnocompany.reminder.location_utilises;

import android.os.Parcel;
import android.os.Parcelable;

public class LocationAlarm implements Parcelable{

    private String id;
    private double latitude;
    private double longitude;
    private float radius;
    private String title;
    private String description;
    private long creator_ID;
    private String creatorName;;


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public long getCreator_ID() {
        return creator_ID;
    }

    public void setCreator_ID(long creator_ID) {
        this.creator_ID = creator_ID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "LocationAlarm{" +
                "id='" + id + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", creator_ID=" + creator_ID +
                ", creatorName='" + creatorName + '\'' +
                '}';
    }

    public LocationAlarm(){}

    public LocationAlarm(Parcel in){
        this.id = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.radius = in.readFloat();
        this.title = in.readString();
        this.description = in.readString();
        this.creator_ID = in.readLong();
        this.creatorName = in.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public LocationAlarm createFromParcel(Parcel in) {
            return new LocationAlarm(in);
        }

        public LocationAlarm[] newArray(int size) {
            return new LocationAlarm[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeFloat(radius);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeLong(creator_ID);
        dest.writeString(creatorName);
    }
}
