package com.cnocompany.reminder.main_uI;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.contacts.ContactsSync;
import com.cnocompany.reminder.date_objects.Reminder;
import com.cnocompany.reminder.list_adapter.NewsFeedAdapter;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.list_adapter.NewsFeedInterface;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewsFeedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,ContactsSync.OnContactsCheckFinish,NewsFeedInterface{


    private static final String LOG = "NewsFeedFragment";
    @Bind(R.id.news_feed_recycler_view) RecyclerView newsFeedRecyclerView;
    @Bind(R.id.news_feed_empty_view) TextView emptyTextView;
    @Bind(R.id.refreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private NewsFeedAdapter newsFeedAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        new ContactsSync(this).execute(getActivity().getApplicationContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_feed_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        newsFeedAdapter = new NewsFeedAdapter(getContext(),this);


        swipeRefreshLayout.setOnRefreshListener(this);

        newsFeedRecyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
        newsFeedRecyclerView.setAdapter(newsFeedAdapter);
        newsFeedRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition = (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled((topRowVerticalPosition >= 0));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRefresh() {

    }


    @Override
    public void onFinish(ArrayList<ContactObject> newContacts) {
        Log.d(LOG,newContacts.toString());
    }

    @Override
    public void onNewFeedItemClick(Reminder clickedReminder) {
        Intent intent = new Intent(getActivity(),ReminderViewerActivity.class);
        intent.putExtra(ReminderViewerActivity.REMINDER_ARG,clickedReminder);
        intent.putExtra(ReminderViewerActivity.REMINDER_TYPE_ARG,ReminderViewerActivity.REMOTE_REMINDER);
        startActivity(intent);
    }
}
