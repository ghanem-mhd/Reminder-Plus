package com.cnocompany.reminder.main_uI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.list_adapter.ContactRemindersAdapter;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.date_objects.Reminder;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CardioidListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RemoteCalendarActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    public  static final String OWNER_ARG = "ownerArg";
    
    public  static final SimpleDateFormat todayTitleFormatter = new SimpleDateFormat("EEEE MMMM dd");

    @Bind(R.id.app_bar) Toolbar toolbar;
    @Bind(R.id.calendar_reminders_list)ListView remindersListView;
    @Bind(R.id.calendar_today_title)   TextView calendarTodayTitle;
    @Bind(R.id.calendar_empty_reminders_list) TextView emptyRemindersList;

    private CaldroidFragment caldroidFragment;
    private final Calendar cal = Calendar.getInstance();
    private Date selectedDate = null;
    private ContactObject reminderContactOwner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity_layout);

        ButterKnife.bind(this);

        if (getIntent() != null){
            reminderContactOwner = getIntent().getParcelableExtra(OWNER_ARG);
        }else finish();
        
        
        setUpToolbar();
        
        caldroidFragment = new CaldroidFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.calendar1, caldroidFragment).commit();
        
        
        caldroidFragment.setCardioidListener(calendarListener);

        
        setListAndAdapter();

    }

    private void setListAndAdapter() {
        ContactRemindersAdapter contactRemindersAdapter = new ContactRemindersAdapter(this);
        remindersListView.setAdapter(contactRemindersAdapter);
        remindersListView.setOnItemClickListener(ReminderClickListener);
    }

    private void setUpToolbar() {
        toolbar.setTitle(reminderContactOwner.getLocalName() +" " + getString(R.string.calendar_label));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private  final CardioidListener calendarListener = new CardioidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
            selectedDate = date;
            calendarTodayTitle.setText(todayTitleFormatter.format(date));
            cal.setTime(date);
            int month = cal.get(Calendar.MONTH)+1;
            caldroidFragment.selectedMonth = month;
            caldroidFragment.selectedYear = cal.get(Calendar.YEAR);

            requstDate(date);
        }

        @Override
        public void onChangeMonth(int month, int year) {
            

        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {
            calendarTodayTitle.setText(todayTitleFormatter.format(new Date()));

        }

    };

    private void requstDate(Date date) {
        emptyRemindersList.setVisibility(View.VISIBLE);
    }


    @Override
    public void onRefresh() {

    }


    private AdapterView.OnItemClickListener ReminderClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            Reminder clickedReminder = (Reminder) adapterView.getItemAtPosition(i);
            Intent intent = new Intent(RemoteCalendarActivity.this,ReminderViewerActivity.class);
            intent.putExtra(ReminderViewerActivity.REMINDER_ARG,clickedReminder);
            intent.putExtra(ReminderViewerActivity.REMINDER_TYPE_ARG,ReminderViewerActivity.REMOTE_REMINDER);
            startActivity(intent);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
