package com.cnocompany.reminder.main_uI;

import android.content.Intent;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.list_adapter.RemindersCursorAdapter;
import com.cnocompany.reminder.R;

import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.cnocompany.reminder.utilises.UtilisesPreferences;
import com.cnocompany.reminder.date_objects.Reminder;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CardioidListener;


import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LocalCalendarFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, Toolbar.OnMenuItemClickListener {

    public  static final SimpleDateFormat formatter = new SimpleDateFormat(Constant.DateTime_Format_IN_DB);
    public  static final SimpleDateFormat todayTitleFormatter = new SimpleDateFormat("EEEE MMMM dd");
    private static final String LOG_TAG         = "ReminderFragment";
    private static final int REMINDERS_LOADER   = 1;

    private final Calendar cal = Calendar.getInstance();
    private CaldroidFragment caldroidFragment;


    @Bind(R.id.calendar_reminders_list)ListView remindersListView;
    @Bind(R.id.calendar_today_title)   TextView calendarTodayTitle;
    @Bind(R.id.calendar_empty_reminders_list) TextView emptyRemindersList;

    Toolbar toolbar;
    RemindersCursorAdapter remindersCursorAdapter;
    UtilisesPreferences utilisesPreferences;
    UserInfoPreferences userInfoPreferences;
    boolean showReminderByOther = true;
    boolean showReminderByYou = true;
    long user_Backend_ID = -1;

    Date selectedDate = new Date();
    Calendar currentlyViewDate = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        caldroidFragment = new CaldroidFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(CaldroidFragment.SELECTED_DATES, true);

        utilisesPreferences = UtilisesPreferences.getInstance(getActivity().getApplicationContext());
        userInfoPreferences = UserInfoPreferences.getInstance(getActivity().getApplicationContext());
        showReminderByYou = utilisesPreferences.getBoolean(UtilisesPreferences.Key.BY_YOU_BOOL, true);
        showReminderByOther = utilisesPreferences.getBoolean(UtilisesPreferences.Key.BY_OTHER_BOOL,true);
        user_Backend_ID = userInfoPreferences.getLong(UserInfoPreferences.USER_BACKEND_ID_LONG);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getFragmentManager().beginTransaction().replace(R.id.calendar1, caldroidFragment).commit();
        caldroidFragment.setCardioidListener(calendarListener);


        remindersCursorAdapter = new RemindersCursorAdapter(getActivity(),null,0);
        remindersListView.setAdapter(remindersCursorAdapter);

        remindersListView.setOnItemClickListener(ReminderClickListener);
        toolbar = (Toolbar) getActivity().findViewById(R.id.contact_viewer_toolbar);

        resetLoader(selectedDate);

    }

    private void setUpToolBarMenu() {
        toolbar = (Toolbar) getActivity().findViewById(R.id.contact_viewer_toolbar);
        toolbar.inflateMenu(R.menu.local_calendar_menu);

        Menu menu = toolbar.getMenu();
        menu.findItem(R.id.local_calendar_menu_by_other_item).setChecked(showReminderByOther);
        menu.findItem(R.id.local_calendar_menu_by_you_item).setChecked(showReminderByYou);

        toolbar.setOnMenuItemClickListener(this);
    }

    private void resetLoader(Date date) {
        Bundle bundle = new Bundle();
        bundle.putString("selectedDate", formatter.format(date));
        getLoaderManager().restartLoader(REMINDERS_LOADER, bundle, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.reminders_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }




    private  final CardioidListener calendarListener = new CardioidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
            selectedDate = date;
            resetLoader(date);
            calendarTodayTitle.setText(todayTitleFormatter.format(date));


            cal.setTime(date);
            int month = cal.get(Calendar.MONTH) + 1;
            int year  = cal.get(Calendar.YEAR) ;
            caldroidFragment.selectedMonth = month;
            caldroidFragment.selectedYear = cal.get(Calendar.YEAR);

            if (!(currentlyViewDate.get(Calendar.MONTH) == month && currentlyViewDate.get(Calendar.YEAR) ==year)){
                caldroidFragment.moveToDate(date);
            }

        }

        @Override
        public void onChangeMonth(int month, int year) {
            toolbar.setTitle(getMonthForInt(month - 1) + " " + year);

            currentlyViewDate.set(Calendar.YEAR, year);
            currentlyViewDate.set(Calendar.MONTH, month);

        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {
            calendarTodayTitle.setText(todayTitleFormatter.format(new Date()));

        }

    };

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String selection = "strftime('%Y-%m-%d'," + ProviderContract.ReminderContract.TIME + ") = strftime('%Y-%m-%d', ? )";
        String selectionArgs  [] =  new String[]{ bundle.getString("selectedDate")};

            return new CursorLoader(getActivity(), ProviderContract.ReminderContract.CONTENT_URI,
                    null,
                    selection,
                    selectionArgs,
                    null);
      /*
        if (showReminderByYou && ! showReminderByOther) {
            selection += " AND " + ProviderContract.ReminderContract.BACKEND_CREATOR_ID + " = ?";
            selectionArgs = new String[]{bundle.getString("selectedDate"), user_Backend_ID + ""};

            return new CursorLoader(getActivity(), ProviderContract.ReminderContract.CONTENT_URI,
                    null,
                    selection,
                    selectionArgs,
                    null);
        }

        if (!showReminderByYou && showReminderByOther) {
            selection += " AND " + ProviderContract.ReminderContract.BACKEND_CREATOR_ID + " != ?";
            selectionArgs = new String[]{bundle.getString("selectedDate"), user_Backend_ID + ""};
            return new CursorLoader(getActivity(), ProviderContract.ReminderContract.CONTENT_URI,
                    null,
                    selection,
                    selectionArgs,
                    null);
        }
        return new CursorLoader(getActivity(), ProviderContract.ReminderContract.CONTENT_URI,
                null,
                null,
                null,
                null);*/
    }



    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        remindersCursorAdapter.swapCursor(cursor);
        if (cursor.getCount() > 0){
            emptyRemindersList.setVisibility(View.GONE);
        }
        else {
            emptyRemindersList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        remindersCursorAdapter.swapCursor(null);
    }

    @Override
    public void onDetach() {
        // Destroys variables and references, and catches Exceptions
        try {
            getLoaderManager().destroyLoader(REMINDERS_LOADER);
            if (remindersCursorAdapter != null) {
                remindersCursorAdapter.changeCursor(null);
                remindersCursorAdapter = null;
                caldroidFragment.dismiss();
            }
        } catch (Throwable localThrowable) {
        }

        // Always call the super method last
        super.onDetach();
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        remindersListView = null;
    }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }

    private AdapterView.OnItemClickListener ReminderClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            CursorWrapper cursorWrapper = (CursorWrapper) adapterView.getItemAtPosition(i);
            Reminder clickedReminder = Reminder.getReminderObjectFromCursor(cursorWrapper);
            Intent intent = new Intent(getActivity(),ReminderViewerActivity.class);
            intent.putExtra(ReminderViewerActivity.REMINDER_ARG,clickedReminder);
            intent.putExtra(ReminderViewerActivity.REMINDER_TYPE_ARG,ReminderViewerActivity.LOCAL_REMINDER);
            startActivity(intent);
        }
    };

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()){
            case R.id.local_calendar_menu_by_you_item:
                item.setChecked(!item.isChecked());
                showReminderByYou = item.isChecked();
                utilisesPreferences.put(UtilisesPreferences.Key.BY_YOU_BOOL,item.isChecked());
                break;

            case R.id.local_calendar_menu_by_other_item:
                item.setChecked(!item.isChecked());
                showReminderByOther = item.isChecked();
                utilisesPreferences.put(UtilisesPreferences.Key.BY_OTHER_BOOL,item.isChecked());
                break;
        }
        resetLoader(selectedDate);
        return false;
    }
}
