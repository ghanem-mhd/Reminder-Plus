package com.cnocompany.reminder.main_uI;



import android.content.Intent;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.list_adapter.PendingReminderCursorAdapter;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.date_objects.Reminder;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PendingReminderFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> ,SwipeRefreshLayout.OnRefreshListener{

    private static final int PENDING_REMINDERS_LOADER = 1;


    @Bind(R.id.pending_reminder_list) ListView pendingRemindersList;
    @Bind(R.id.pending_reminder_empty_text) TextView emptyText;
    @Bind(R.id.refreshLayout) SwipeRefreshLayout swipeRefreshLayout;


    PendingReminderCursorAdapter pendingReminderCursorAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Reminder reminder = new Reminder();


        reminder.setTitle("Home!");
        reminder.setDescription("test sdlk klka dfdkj adfkj dffasdf adfklj ");
        reminder.setTime("2015-10-11 15:10");
        reminder.setType(2);

        reminder.setIsSilent(1);
        reminder.setLat(33.430163);
        reminder.setLng(36.156304);
        reminder.setRadius(500);


        reminder.setBackendCreatorID(3);
        reminder.setBackendReminderID(9);
        reminder.setBackendUserReminderID(10);
        reminder.setParticipantIDS(1l);
        reminder.setStatus(1);


        Reminder.AddReminderToContentProvider(getActivity(), reminder);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pending_reminder_list_layout,container,false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pendingReminderCursorAdapter = new PendingReminderCursorAdapter(getActivity(),null,0);
        pendingRemindersList.setAdapter(pendingReminderCursorAdapter);
        pendingRemindersList.setOnItemClickListener(OnPendingReminderClickListener);

        getLoaderManager().initLoader(PENDING_REMINDERS_LOADER, null, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ProviderContract.ReminderContract.STATUS + " = ?";
        String selectionARGS [] = new String[]{ ProviderContract.ReminderContract.PENDING_REMINDERS };
        return new CursorLoader(getActivity().getApplicationContext(),ProviderContract.ReminderContract.CONTENT_URI,null,selection,selectionARGS,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        pendingReminderCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        pendingReminderCursorAdapter.swapCursor(null);
    }


    @Override
    public void onDetach() {
        try {
            getLoaderManager().destroyLoader(PENDING_REMINDERS_LOADER);
            if (pendingReminderCursorAdapter != null) {
                pendingReminderCursorAdapter.changeCursor(null);
                pendingReminderCursorAdapter = null;
            }
        } catch (Throwable localThrowable) {
            localThrowable.printStackTrace();
        }
        super.onDetach();
    }

    private AdapterView.OnItemClickListener OnPendingReminderClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CursorWrapper cursorWrapper = (CursorWrapper) parent.getItemAtPosition(position);
            Reminder clickedReminder = Reminder.getReminderObjectFromCursor(cursorWrapper);

            Intent intent = new Intent(getActivity(),PendingReminderViewerActivity.class);
            intent.putExtra(PendingReminderViewerActivity.REMINDER_ARG,clickedReminder);
            startActivity(intent);
        }
    };

    @Override
    public void onRefresh() {

    }
}
