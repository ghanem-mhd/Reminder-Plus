package com.cnocompany.reminder.main_uI;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.R;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactViewerActivity2 extends AppCompatActivity  {


    public static final String CONTACT_OBJECT_ARG = "contactObjectArg";

    @Bind(R.id.app_bar) Toolbar toolbar;
    @Bind(R.id.contact_viewer_contact_image) ImageView contactImage;
    @Bind(R.id.contact_viewer_conatact_phone_number) TextView phoneNumber;
    @Bind(R.id.contact_viewer_conatact_phone_number_layout) LinearLayout phoneNumberLayout;
    @Bind(R.id.contact_viewer_time_reminders_layout) LinearLayout timeReminderLayout;
    @Bind(R.id.contact_viewer_location_reminders_layout) LinearLayout locationReminderLayout;
    private PhoneNumberUtil phoneNumberUtil;

    private ContactObject viewedContact = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_viewer_layout_2);

        ButterKnife.bind(this);
        phoneNumberUtil = PhoneNumberUtil.getInstance();
        setUpToolbar();

        setContactInfo();

        changeIconColor();
    }


    private void setContactInfo() {
        Picasso.with(this).load(viewedContact.getPhotoPath()).into(contactImage);
        phoneNumber.setText("+" + viewedContact.getPhoneNumber());
    }

    private void setUpToolbar() {
        Parcelable contactsObjectArg = getIntent().getParcelableExtra(CONTACT_OBJECT_ARG);
        if (getIntent() != null &&  contactsObjectArg!= null){
            viewedContact = (ContactObject) contactsObjectArg;
            toolbar.setTitle(viewedContact.getLocalName());
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }else{
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contact_menu, menu);

        if (viewedContact.isTrusted()){
            menu.findItem(R.id.contact_menu_authenticity).setTitle(getString(R.string.remove_from_trust_label));

        }else{
            menu.findItem(R.id.contact_menu_authenticity).setTitle(getString(R.string.add_to_trust_label));

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.contact_menu_authenticity:
                if (viewedContact.isTrusted()){
                    changeAuthenticity(0);
                    viewedContact.setIsTrusted(false);
                    item.setTitle(getString(R.string.add_to_trust_label));
                }else{
                    viewedContact.setIsTrusted(true);
                    changeAuthenticity(1);
                    item.setTitle(getString(R.string.remove_from_trust_label));
                }
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }



    private void changeIconColor() {
        int accentColor = Color.parseColor("#448AFF");
        ImageView fullReminderIcon1 = (ImageView) findViewById(R.id.contact_viewer_icon_1);
        ImageView fullReminderIcon2 = (ImageView) findViewById(R.id.contact_viewer_icon_2);
        ImageView fullReminderIcon3 = (ImageView) findViewById(R.id.contact_viewer_icon_3);
        fullReminderIcon1.setColorFilter(accentColor);
        fullReminderIcon2.setColorFilter(accentColor);
        fullReminderIcon3.setColorFilter(accentColor);
    }


    @OnClick(R.id.contact_viewer_contact_image)
    public  void viewContactPhoto(){
        Intent intent = new Intent(ContactViewerActivity2.this,FullScreenImageViewerActivity.class);
        intent.putExtra(FullScreenImageViewerActivity.PATH_ARG,viewedContact.getPhotoPath().toString());
        intent.putExtra(FullScreenImageViewerActivity.TITLE_ARG,viewedContact.getLocalName());
        startActivity(intent);
    }

    @OnClick(R.id.contact_viewer_conatact_phone_number_layout)
    public  void phoneNumber(){

    }

    @OnClick(R.id.contact_viewer_time_reminders_layout)
    public  void viewContactCalendar(){
        Intent intent = new Intent(this,RemoteCalendarActivity.class);
        intent.putExtra(RemoteCalendarActivity.OWNER_ARG,viewedContact);
        startActivity(intent);
    }

    @OnClick(R.id.contact_viewer_location_reminders_layout)
    public  void viewContactMap(){

    }

    private void changeAuthenticity(int flag) {
        String selection = ProviderContract.ContactContract.BACKEND_USER_ID + " = ?";
        String [] selectionARGS = new String[]{viewedContact.getBackendID()+""};
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProviderContract.ContactContract.TRUSTED_CONTACT_BOOL, flag);
        getContentResolver().update(ProviderContract.ContactContract.CONTENT_URI, contentValues, selection, selectionARGS);
        if (flag ==1){
            Toast.makeText(this,viewedContact.getLocalName()+" added to trusted list",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this,viewedContact.getLocalName()+" removed from trusted list",Toast.LENGTH_LONG).show();
        }
    }

}
