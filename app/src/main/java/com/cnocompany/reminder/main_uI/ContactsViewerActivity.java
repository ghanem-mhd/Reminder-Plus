package com.cnocompany.reminder.main_uI;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.R;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ContactsViewerActivity extends AppCompatActivity{

    public static final String CONTACT_OBJECT_ARG = "contactObjectArg";

    @Bind(R.id.contact_viewer_toolbar) Toolbar toolbar;
    @Bind(R.id.contact_viewer_collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.contact_viewer_contact_image) ImageView contactImage;
    @Bind(R.id.contact_viewer_conatact_phone_number) TextView phoneNumber;
    private ContactObject viewedContact = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_viewer_layout);

        ButterKnife.bind(this);
        setUpToolbar();
        setContactInfo();
        changeIconColor();




    }

    private void setContactInfo() {
        Picasso.with(this).load(viewedContact.getPhotoPath()).into(contactImage);
        phoneNumber.setText(viewedContact.getPhoneNumber());
    }

    private void setUpToolbar() {
        Parcelable contactsObjectArg = getIntent().getParcelableExtra(CONTACT_OBJECT_ARG);
        if (getIntent() != null &&  contactsObjectArg!= null){
            viewedContact = (ContactObject) contactsObjectArg;
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        }else{
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }


    private void changeIconColor() {
        int accentColor = Color.parseColor("#448AFF");
        ImageView fullReminderIcon1 = (ImageView) findViewById(R.id.contact_viewer_icon_1);

        fullReminderIcon1.setColorFilter(accentColor);

    }
}
