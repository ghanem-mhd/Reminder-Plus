package com.cnocompany.reminder.main_uI;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.cnocompany.reminder.helper.TouchImageView;
import com.cnocompany.reminder.R;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FullScreenImageViewerActivity extends AppCompatActivity{
    public static final String TITLE_ARG = "title";
    public static final String PATH_ARG  = "path";

    @Bind(R.id.app_bar) Toolbar toolbar;
    @Bind(R.id.full_image_image) TouchImageView image;

    private String activityTitle = null;
    private Uri imagePath     = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen_image_layout);

        ButterKnife.bind(this);


        if (getIntent() != null){
            activityTitle = getIntent().getStringExtra(TITLE_ARG);
            imagePath     = Uri.parse(getIntent().getStringExtra(PATH_ARG));
        }else finish();

        toolbar.setTitle(activityTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Picasso.with(this).load(imagePath).into(image);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
