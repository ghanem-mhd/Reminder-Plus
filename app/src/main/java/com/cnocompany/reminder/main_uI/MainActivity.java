package com.cnocompany.reminder.main_uI;



import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cnocompany.reminder.BuildConfig;
import com.cnocompany.reminder.content_provider.PendingRemindersLoader;
import com.cnocompany.reminder.gem_utilises.RegistrationIntentService;
import com.cnocompany.reminder.helper.DialogManager;
import com.cnocompany.reminder.utilises.CircularImageView;
import com.cnocompany.reminder.helper.RoundedBackgroundSpan;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;


import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener,PendingRemindersLoader.OnPendingRemindersLoaderFinish, View.OnClickListener {

    private static final String TAG = "MainAcitivity";

    @Bind(R.id.contact_viewer_toolbar) Toolbar toolbar;
    @Bind(R.id.drawer) DrawerLayout drawerLayout;
    @Bind(R.id.navigation_view) NavigationView navigationView;
    @Bind(R.id.user_name) TextView userName;
    @Bind(R.id.user_photo) CircularImageView userPhoto;
    @Bind(R.id.user_edit_button) ImageButton editUserInfoButton;

    private UserInfoPreferences userInfoPreferences;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        ButterKnife.bind(this);


        navigationView.setNavigationItemSelectedListener(monNavigationItemSelectedListener);
        setSupportActionBar(toolbar);
        setActionBarDrawerToggle();
        setFragment(R.id.drawer_menu_news_feed);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        editUserInfoButton.setOnClickListener(this);
        userInfoPreferences = UserInfoPreferences.getInstance(this.getApplicationContext());

        imageLoader = ImageLoader.getInstance();


        displayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_contact_picture_holo_light)
                .showImageForEmptyUri(R.drawable.ic_contact_picture_holo_light)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }

    private void setUpUserInfo() {
        userName.setText(userInfoPreferences.getString(UserInfoPreferences.USER_REMOTE_NAME));
        imageLoader.displayImage("http://185.3.152.227/api/image/user/" + 5, userPhoto, displayImageOptions);
    }

    @Override
    protected void onResume() {
        super.onResume();
        queryPendingReminderCount();
        setUpUserInfo();
    }

    private void queryPendingReminderCount() {
        new PendingRemindersLoader(this).execute(getApplicationContext());
    }



    private void setUpPendingReminderCount(String pendingRemindersCount) {
        MenuItem pendingReminderMenuItem = navigationView.getMenu().findItem(R.id.drawer_menu_pending_reminders);
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder("Pending Reminders"+"             ");
        stringBuilder.append(pendingRemindersCount);
        stringBuilder.setSpan(new RoundedBackgroundSpan(), 30, 30 + pendingRemindersCount.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        pendingReminderMenuItem.setTitle(stringBuilder);
        if (BuildConfig.DEBUG){
            Log.d(TAG,"Finish query and setUp with count = "+pendingRemindersCount);
        }



    }


    private NavigationView.OnNavigationItemSelectedListener monNavigationItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(final MenuItem menuItem) {
            drawerLayout.closeDrawers();
            queryPendingReminderCount();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbar.getMenu().clear();
                    setFragment(menuItem.getItemId());
                }
            },350);

            return true;
        }
    };

    private void setActionBarDrawerToggle(){
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }


            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };

        //Setting the actionbarToggle to drawer_menu layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }


    private void setFragment(int fragmentIndex){
        final android.support.v4.app.FragmentTransaction fragmentTransactionSupport = getSupportFragmentManager().beginTransaction();

        switch (fragmentIndex){
            case R.id.drawer_menu_news_feed:
                toolbar.setTitle(R.string.drawer_news_feed);
                fragmentTransactionSupport.replace(R.id.frame, new NewsFeedFragment());
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransactionSupport.commit();
                break;

            case R.id.drawer_menu_new_reminder:
                toolbar.setTitle(R.string.drawer_new_reminder);
                getSupportFragmentManager().popBackStack();
                fragmentTransactionSupport.add(R.id.frame, new NewReminderFragment(), "new_reminder");
                fragmentTransactionSupport.addToBackStack(null);
                fragmentTransactionSupport.commit();
                break;

            case R.id.drawer_menu_calendar:
                toolbar.setTitle(R.string.calendar_label);
                getSupportFragmentManager().popBackStack();
                fragmentTransactionSupport.add(R.id.frame, new LocalCalendarFragment(), "calendar");
                fragmentTransactionSupport.addToBackStack(null);
                fragmentTransactionSupport.commit();
                break;


            case R.id.drawer_menu_map:
                toolbar.setTitle(R.string.drawer_map);
                getSupportFragmentManager().popBackStack();
                fragmentTransactionSupport.add(R.id.frame, new LocalMapFragment(), "map");
                fragmentTransactionSupport.addToBackStack(null);
                fragmentTransactionSupport.commit();
                break;

            case R.id.drawer_menu_contacts:
                toolbar.setTitle(R.string.drawer_contacts);
                getSupportFragmentManager().popBackStack();
                fragmentTransactionSupport.add(R.id.frame, new ContactsFragment(), "contacts");
                fragmentTransactionSupport.addToBackStack(null);
                fragmentTransactionSupport.commit();
                break;

            case R.id.drawer_menu_setting:
                toolbar.setTitle(R.string.drawer_setting);
                getSupportFragmentManager().popBackStack();
                fragmentTransactionSupport.add(R.id.frame, new SettingFragment(), "settings");
                fragmentTransactionSupport.addToBackStack(null);
                fragmentTransactionSupport.commit();
                break;


            case R.id.drawer_menu_pending_reminders:
                toolbar.setTitle(R.string.drawer_pending_reminders);
                getSupportFragmentManager().popBackStack();
                fragmentTransactionSupport.add(R.id.frame, new PendingReminderFragment(), "pendingReminder");
                fragmentTransactionSupport.addToBackStack(null);
                fragmentTransactionSupport.commit();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);

        if (fragment instanceof NewsFeedFragment) {
            toolbar.setTitle(R.string.drawer_news_feed);
            return;
        }

        if (fragment instanceof NewReminderFragment) {
            toolbar.setTitle(R.string.drawer_new_reminder);
            return;
        }

        if (fragment instanceof LocalMapFragment) {
            toolbar.setTitle(R.string.drawer_map);
            return;
        }

        if (fragment instanceof LocalCalendarFragment) {
            toolbar.setTitle(R.string.calendar_label);
            return;
        }

        if (fragment instanceof ContactsFragment) {
            toolbar.setTitle(R.string.drawer_contacts);
            return;
        }


        if (fragment instanceof PendingReminderFragment){
            toolbar.setTitle(R.string.drawer_pending_reminders);
        }
    }

    @Override
    public void OnFinish(String pendingReminderCount) {
        if (!pendingReminderCount.equals("0")){
            setUpPendingReminderCount(pendingReminderCount);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.user_edit_button:
                Intent intent = new Intent(this,EditUserInfoActivity.class);
                startActivity(intent);
                break;
        }
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                //todo show message
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

}