package com.cnocompany.reminder.main_uI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.cnocompany.reminder.helper.DialogManager;
import com.cnocompany.reminder.helper.PhotoManager;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.rey.material.widget.Button;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditUserInfoActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String REG_REQ_ARG = "register_req_arg";
    private static final int SELECT_PICTURE_REQUEST_CODE = 1;
    private static final int RESIZE_PICTURE_REQUEST_CODE = 2;

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.edit_user_photo) ImageView userImage;
    @Bind(R.id.edit_user_name_edit_text) EditText userNameEditText;
    @Bind(R.id.edit_user_select_photo_button) Button selectPhotoButton;
    @Bind(R.id.edit_user_remove_photo_button) Button removePhotoButton;

    private UserInfoPreferences userInfoPreferences;

    private Uri selectedPhotoUri;
    private ProgressDialog dialog;

    private boolean photoRemoved;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    private boolean isRegestrationRequst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_activity_layout);

        userInfoPreferences = UserInfoPreferences.getInstance(this);

        isRegestrationRequst = getIntent().getBooleanExtra(REG_REQ_ARG,false);

        ButterKnife.bind(this);


        selectPhotoButton.setOnClickListener(this);
        removePhotoButton.setOnClickListener(this);



        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            if (!isRegestrationRequst)
                actionBar.setDisplayHomeAsUpEnabled(true);
        }


        dialog = DialogManager.getProgressDialog(this,"Loading...");

        imageLoader = ImageLoader.getInstance();


        displayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_contact_picture_holo_light)
                .showImageForEmptyUri(R.drawable.ic_contact_picture_holo_light)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        setInfo();
    }

    private void setInfo() {
        userNameEditText.setText(userInfoPreferences.getString(UserInfoPreferences.USER_REMOTE_NAME));
        imageLoader.displayImage("http://185.3.152.227/api/image/user/" + 5, userImage, displayImageOptions);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_menu_save_item:

                dialog.show();
                if (selectedPhotoUri != null ){
                    try {
                        File userPhoto = PhotoManager.createImageFile(-1);
                        invalidatePhoto();
                        userInfoPreferences.put(UserInfoPreferences.USER_PHOTO_PATH, Uri.fromFile(userPhoto).toString());
                        PhotoManager.savePhotoAndDeleteTemp(this, selectedPhotoUri, userPhoto);

                        //todo ................


                    } catch (IOException e) {e.printStackTrace();}
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();


                        setResult(RESULT_OK);
                        finish();
                    }
                }, 5000);
                userInfoPreferences.put(UserInfoPreferences.USER_REMOTE_NAME, userNameEditText.getText().toString().trim());
                return true;

            case android.R.id.home:
                if (checkInfoChanged()){
                    DialogManager.ShowConfirmDialog(this,"Are you sure you want to discard the change?","Discard","Keep Editing");
                }else {
                    finish();
                }
                break;
        }


        return false;
    }

    private void invalidatePhoto() {
        File imageFile = imageLoader.getDiskCache().get("http://185.3.152.227/api/image/user/" + 5);
        if (imageFile.exists()) {
            imageFile.delete();
        }
        MemoryCacheUtils.removeFromCache("http://185.3.152.227/api/image/user/" + 5, imageLoader.getMemoryCache());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_user_select_photo_button:
                Intent chooserIntent = getPickImageChooserIntent();
                if (chooserIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(chooserIntent, SELECT_PICTURE_REQUEST_CODE);
                }
                break;

            case R.id.edit_user_remove_photo_button:
                photoRemoved = true;
                userInfoPreferences.remove(UserInfoPreferences.USER_PHOTO_PATH);
                invalidatePhoto();
                imageLoader.displayImage(null,userImage,displayImageOptions);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_PICTURE_REQUEST_CODE){
                try {
                    setPickImageResultUri(data);
                    Intent intent = new Intent(this,ResizeImageActivity.class);
                    intent.putExtra(ResizeImageActivity.INPUT_PHOTO_URI_ARG, selectedPhotoUri);
                    startActivityForResult(intent,RESIZE_PICTURE_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            if (requestCode == RESIZE_PICTURE_REQUEST_CODE){
                photoRemoved = false;
                userImage.setImageURI(selectedPhotoUri);
            }
        }
    }

    public Intent getPickImageChooserIntent() {

        try {
            selectedPhotoUri = Uri.fromFile(PhotoManager.createTempImageFile(-1));
            List<Intent> allIntents = new ArrayList<>();
            PackageManager packageManager = getPackageManager();

            // collect all camera intents
            Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for (ResolveInfo res : listCam) {
                Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(res.activityInfo.packageName);
                if (selectedPhotoUri != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedPhotoUri);
                }
                allIntents.add(intent);
            }

            // collect all gallery intents
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            galleryIntent.setType("image/*");
            List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
            for (ResolveInfo res : listGallery) {
                Intent intent = new Intent(galleryIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(res.activityInfo.packageName);
                allIntents.add(intent);
            }

            // the main intent is the last in the  list (fucking android) so pickup the useless one
            Intent mainIntent = allIntents.get(allIntents.size() - 1);
            for (Intent intent : allIntents) {
                if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                    mainIntent = intent;
                    break;
                }
            }
            allIntents.remove(mainIntent);

            // Create a chooser from the main  intent
            Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

            // Add all other intents
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

            return chooserIntent;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Uri setPickImageResultUri(Intent  data) throws IOException {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        if (isCamera){
            return selectedPhotoUri;
        }else {
            File tempFile = PhotoManager.createTempImageFile(-1); //create temp file in RP directory because user select image from gallary
            selectedPhotoUri = PhotoManager.savePhotoFromGallary(this, data.getData(), tempFile);   // return the uri of temp file
            return selectedPhotoUri;
        }
    }

    private boolean checkInfoChanged(){
        return (photoRemoved || selectedPhotoUri!=null || !userNameEditText.getText().toString().equals(userInfoPreferences.getString(UserInfoPreferences.USER_REMOTE_NAME)));
    }

}
