package com.cnocompany.reminder.main_uI;


import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UtilisesPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class LocalMapFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, Toolbar.OnMenuItemClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private static final String LOG_TAG = "LocalMap";
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private GoogleMap googleMap;
    private MapView mMapView;
    private Toolbar toolbar;

    //this map will link marker with reminder id to use later when user click on marker
    HashMap<Marker,Long> hashMap = new HashMap<>();
    ArrayList<LatLng> latLngs = new ArrayList<>();
    private UtilisesPreferences utilisesPreferences;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        utilisesPreferences = UtilisesPreferences.getInstance(getActivity().getApplicationContext());
    }

    private void queryMarkers() {
        Cursor cursor = getActivity().getContentResolver().query(ProviderContract.LocationAlarmContract.CONTENT_URI,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title(cursor.getString(cursor.getColumnIndex(ProviderContract.LocationAlarmContract.TITLE)));
                double lat = Double.parseDouble(cursor.getString(cursor.getColumnIndex(ProviderContract.LocationAlarmContract.LAT)));
                double lng = Double.parseDouble(cursor.getString(cursor.getColumnIndex(ProviderContract.LocationAlarmContract.LNG)));
                LatLng markerLatLng = new LatLng(lat,lng);
                latLngs.add(markerLatLng);
                markerOptions.position(markerLatLng);
                Marker marker = googleMap.addMarker(markerOptions);
                hashMap.put(marker,cursor.getLong(cursor.getColumnIndex(ProviderContract.LocationAlarmContract._ID)));
            }while (cursor.moveToNext());
        }
        cursor.close();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.local_map_layout,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        mMapView.getMapAsync(this);

        toolbar = (Toolbar) getActivity().findViewById(R.id.contact_viewer_toolbar);
        toolbar.inflateMenu(R.menu.map_menu);
        toolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d(LOG_TAG, mLastLocation.toString());
            //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 16));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
        this.googleMap = googleMap;

        queryMarkers();



        if (mMapView.getViewTreeObserver().isAlive()) {
            mMapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {

                    if (latLngs.size()>0){
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (LatLng latLng:latLngs){
                            builder.include(latLng);
                        }
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mMapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mMapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                    }
                }
            });

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        long markerID = utilisesPreferences.getLong(UtilisesPreferences.Key.MARKER_DELETED,-1);
        if (markerID != -1){
            utilisesPreferences.remove(UtilisesPreferences.Key.MARKER_DELETED);
            for (Map.Entry<Marker, Long> entry : hashMap.entrySet()) {
                if (entry.getValue().equals(markerID)) {
                    entry.getKey().remove();
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.map_menu_normal:
                if (googleMap.getMapType() != GoogleMap.MAP_TYPE_NORMAL){
                    item.setChecked(true);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
                return true;
            case R.id.map_menu_hybrid:
                if (googleMap.getMapType() != GoogleMap.MAP_TYPE_HYBRID){
                    item.setChecked(true);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                }
                return true;
            case R.id.map_menu_satellite:
                if (googleMap.getMapType() != GoogleMap.MAP_TYPE_SATELLITE){
                    item.setChecked(true);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
                return true;
        }
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(getActivity(),ReminderViewerActivity.class);
        intent.putExtra(ReminderViewerActivity.REMINDER_ID_ARG,hashMap.get(marker));
        intent.putExtra(ReminderViewerActivity.REMINDER_TYPE_ARG,ReminderViewerActivity.LOCAL_REMINDER);
        startActivity(intent);
    }
}
