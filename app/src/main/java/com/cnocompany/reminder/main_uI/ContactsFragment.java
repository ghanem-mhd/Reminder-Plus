package com.cnocompany.reminder.main_uI;

import android.content.Intent;
import android.database.CursorWrapper;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;


import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.contacts.ContactsSync;
import com.cnocompany.reminder.content_provider.ProviderContract;

import com.cnocompany.reminder.list_adapter.ContactsCursorAdapter;
import com.cnocompany.reminder.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ContactsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,SwipeRefreshLayout.OnRefreshListener,ContactsSync.OnContactsCheckFinish, Toolbar.OnMenuItemClickListener

{
    private static final int CONTACTS_LOADER = 0;
    private static final String LOG_TAG = "ContactsFragment";
    @Bind(R.id.contact_listview) ListView contactsList;
    @Bind(R.id.refreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private ContactsCursorAdapter contactsCursorAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.contacts_fragment, container, false);

        ButterKnife.bind(this,rootView);


        contactsCursorAdapter = new ContactsCursorAdapter(getActivity(),null,0);
        contactsList.setAdapter(contactsCursorAdapter);


        swipeRefreshLayout.setOnRefreshListener(this);
        contactsList.setOnItemClickListener(onContactClickListener);
        contactsList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (view == null || view.getChildCount() == 0) ? 0 : view.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled((topRowVerticalPosition >= 0));
            }
        });

        getLoaderManager().initLoader(CONTACTS_LOADER, null, this);


        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.contact_viewer_toolbar);
        toolbar.inflateMenu(R.menu.contacts_menu);
        toolbar.setOnMenuItemClickListener(this);
        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {
    return new CursorLoader(getActivity(),
            ProviderContract.ContactContract.CONTENT_URI,
            null,
            null,
            null,
            ProviderContract.ContactContract.LOCAL_NAME_STR + " ASC ");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        contactsCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        contactsCursorAdapter.swapCursor(null);
    }


    @Override
    public void onDestroyView() {
        contactsList = null;
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        try {
            getLoaderManager().destroyLoader(CONTACTS_LOADER);
            if (contactsCursorAdapter != null) {
                contactsCursorAdapter.changeCursor(null);
                contactsCursorAdapter = null;
            }
        } catch (Throwable ignored) {}
        super.onDetach();
    }

    private AdapterView.OnItemClickListener onContactClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            CursorWrapper cursorWrapper = (CursorWrapper) adapterView.getItemAtPosition(i);
            if (cursorWrapper.getInt(cursorWrapper.getColumnIndex(ProviderContract.ContactContract.RP_USER_BOOL))==1){
                ContactObject contactObject = new ContactObject();
                contactObject.setId(cursorWrapper.getLong(cursorWrapper.getColumnIndex(ProviderContract.ContactContract._ID)));
                contactObject.setLocalName(cursorWrapper.getString(cursorWrapper.getColumnIndex(ProviderContract.ContactContract.LOCAL_NAME_STR)));
                contactObject.setPhotoPath(Uri.parse(cursorWrapper.getString(cursorWrapper.getColumnIndex(ProviderContract.ContactContract.PHOTO_PATH))));
                contactObject.setBackendID(cursorWrapper.getInt(cursorWrapper.getColumnIndex(ProviderContract.ContactContract.BACKEND_USER_ID)));
                contactObject.setPhoneNumber(cursorWrapper.getString(cursorWrapper.getColumnIndex(ProviderContract.ContactContract.PHONE_STR)));
                contactObject.setIsTrusted(cursorWrapper.getInt(cursorWrapper.getColumnIndex(ProviderContract.ContactContract.TRUSTED_CONTACT_BOOL)) == 1);
                Intent intent = new Intent(getActivity(),ContactViewerActivity2.class);
                intent.putExtra(ContactsViewerActivity.CONTACT_OBJECT_ARG, contactObject);
                startActivity(intent);
            }
        }
    };

    @Override
    public void onRefresh() {
        refreshContacts();
    }

    @Override
    public void onFinish(ArrayList<ContactObject> newContacts) {
        Log.d(LOG_TAG,newContacts.toString());
        swipeRefreshLayout.setRefreshing(false);
    }

    private void refreshContacts(){
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        new ContactsSync(this).execute(getActivity());
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contacts_menu_refresh_action:
                refreshContacts();
                return true;
        }
        return false;
    }
}
