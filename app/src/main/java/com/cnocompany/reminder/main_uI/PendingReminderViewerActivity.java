package com.cnocompany.reminder.main_uI;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.contacts.ContactsManager;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.cnocompany.reminder.utilises.UtilisesPreferences;
import com.cnocompany.reminder.date_objects.Reminder;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.hoang8f.android.segmented.SegmentedGroup;

public class PendingReminderViewerActivity extends AppCompatActivity{
    public static final String REMINDER_ARG = "reminderArg";
    public static final String TAG = "pendingView";



    @Bind(R.id.full_reminder_toolbar) Toolbar toolbar;
    @Bind(R.id.pending_reminder_viewer_title)         TextView reminderTitle;
    @Bind(R.id.pending_reminder_viewer_description)   TextView  reminderDescription;
    @Bind(R.id.pending_reminder_viewer_time)          TextView  reminderTime;
    @Bind(R.id.pending_reminder_viewer_location)      TextView reminderLocation;

    @Bind(R.id.pending_reminder_viewer_participators) LinearLayout participatorsLayout;
    @Bind(R.id.pending_reminder_viewer_time_layout) LinearLayout timeLayout;
    @Bind(R.id.pending_reminder_viewer_location_layout) LinearLayout locationLayout;
    @Bind(R.id.pending_reminder_privacy_radio_group) SegmentedGroup privacySegmentGroup;

    private LayoutInflater layoutInflater;
    private UtilisesPreferences utilisesPreferences;
    private UserInfoPreferences userInfoPreferences;
    private long userBackendID = -1;
    private Reminder viewedReminder;
    private int reminderStatus = -1 ; //-1 mean not determined - 1 public - 2 private

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pending_reminder_viewer_layout);
        ButterKnife.bind(this);
        layoutInflater =(LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


        if (getIntent() != null){
            viewedReminder = (Reminder) getIntent().getSerializableExtra(REMINDER_ARG);
        }else finish();

        setUpToolbar();

        changeIconColor();


        try {
            setReminderToUI();
            queryParticipant();
        } catch (ParseException e) {
            e.printStackTrace();
            finish();
        }

        utilisesPreferences = UtilisesPreferences.getInstance(this.getApplicationContext());
        userInfoPreferences = UserInfoPreferences.getInstance(this.getApplicationContext());
        userBackendID       = userInfoPreferences.getLong(UserInfoPreferences.USER_BACKEND_ID_LONG, -1);


        privacySegmentGroup.setOnCheckedChangeListener(privacyCheckListener);
    }

    private void setReminderToUI() throws ParseException {
        reminderTitle.setText(viewedReminder.getTitle());
        reminderDescription.setText(viewedReminder.getDescription());

        if (viewedReminder.getType() == ProviderContract.ReminderContract.TIME_REMINDER){
            timeLayout.setVisibility(View.VISIBLE);
            Date reminderDate =  Constant.DB_formatter.parse(viewedReminder.getTime());
            CharSequence datePartString = DateUtils.getRelativeDateTimeString(this, reminderDate.getTime(), DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS, DateUtils.FORMAT_ABBREV_WEEKDAY);
            reminderTime.setText(datePartString.toString());
        }else{
            locationLayout.setVisibility(View.VISIBLE);
            reminderLocation.setText(viewedReminder.getAddress());
        }
    }

    private void queryParticipant(){
        String selection = ProviderContract.ContactReminderContract.BACKEND_REMINDER_ID + " = ?";
        Cursor cursor = getContentResolver().query(ProviderContract.ContactReminderContract.CONTENT_URI, null, selection, new String[]{viewedReminder.getBackendReminderID() + ""}, null);
        if (cursor.moveToFirst()){
            do{
                addLocalParticipantItemToUI(cursor.getInt(cursor.getColumnIndex(ProviderContract.ContactReminderContract.BACKEND_CONTACT_ID)), false);
            }while (cursor.moveToNext());
        }
        cursor.close();

    }

    private void addLocalParticipantItemToUI(long userID, boolean isCreator){
        View participantView = layoutInflater.inflate(R.layout.participant_item,null);
        participantView.setId((int) userID);
        ButterKnife.bind(participantView);

        TextView  participantName    = ButterKnife.findById(participantView,R.id.participant_item_name);
        TextView  participantStatus  = ButterKnife.findById(participantView,R.id.participant_item_status);
        ImageView participantPhoto   = ButterKnife.findById(participantView,R.id.participant_item_photo);


        if (isCreator){
            participantStatus.setVisibility(View.VISIBLE);
            participantStatus.setText("Creator");
        }

        ContactObject contactObject = ContactsManager.getContactByBackendID(getApplicationContext(), userID);
        if (contactObject != null) {
            participantName.setText(contactObject.getLocalName());
            Picasso.with(getApplicationContext()).load(contactObject.getPhotoPath()).into(participantPhoto);
            participatorsLayout.addView(participantView);
            participantView.setOnClickListener(onParticipantClickListener);
        }
        ButterKnife.unbind(participantView);
    }



    private void setUpToolbar() {
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("");
    }


    private void changeIconColor() {
        int accentColor = Color.parseColor("#448AFF");
        ImageView fullReminderIcon1 = (ImageView) findViewById(R.id.pending_reminder_viewer_icon_1);
        ImageView fullReminderIcon2 = (ImageView) findViewById(R.id.pending_reminder_viewer_icon_2);
        ImageView fullReminderIcon3 = (ImageView) findViewById(R.id.pending_reminder_viewer_icon_3);
        ImageView fullReminderIcon4 = (ImageView) findViewById(R.id.pending_reminder_viewer_icon_4);
        fullReminderIcon1.setColorFilter(accentColor);
        fullReminderIcon2.setColorFilter(accentColor);
        fullReminderIcon3.setColorFilter(accentColor);
        fullReminderIcon4.setColorFilter(accentColor);
    }

    private View.OnClickListener onParticipantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("test", String.valueOf(view.getId()));
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pending_reminder_view_menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            case R.id.pending_reminder_menu_accept_item:

                return true;

            case R.id.pending_reminder_menu_reject_item:

                return true;

            case R.id.pending_reminder_menu_block_sender:

                return true;


        }
        return false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }


    private RadioGroup.OnCheckedChangeListener privacyCheckListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == R.id.pending_reminder_private){

            }

            if (checkedId == R.id.pending_reminder_public){

            }
        }
    };
}
