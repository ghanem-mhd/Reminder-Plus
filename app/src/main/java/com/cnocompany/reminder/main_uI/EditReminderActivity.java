package com.cnocompany.reminder.main_uI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cnocompany.reminder.R;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.date_objects.Reminder;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.helper.ContactDetail;
import com.cnocompany.reminder.helper.ContactEditText;
import com.cnocompany.reminder.helper.DialogManager;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.rey.material.widget.Button;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.hoang8f.android.segmented.SegmentedGroup;

/**This class will edit an exist reminder created by the user
 * **/
public class EditReminderActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, View.OnClickListener, TextWatcher {


    private static final int    LOCATION_REMINDER          = 0;
    private static final int    TIME_REMINDER              = 1;
    private static final int    PLACE_PICKER_REQUEST = 1;

    public static final String REMINDER_ID_ARG = "reminder_id_arg";
    public static final String REMINDER_ARG    = "reminder_arg";

    Reminder reminder;

    GregorianCalendar dateAndTimeGC;
    private int reminderType;
    Long reminderTime ;
    private boolean locationChanged,dateChanged,timeChanged, participantChanged;

    private ProgressDialog progressDialog;
    private ContactDetail [] newParticipants;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_reminder_layout);
        ButterKnife.bind(this);


        Long reminderID = getIntent().getLongExtra(REMINDER_ID_ARG,-1);
        reminder = (Reminder) getIntent().getSerializableExtra(REMINDER_ARG);


        if (reminder == null){ // reminderId the given
            reminder = QueryReminder(reminderID);
            if (reminder == null){ //error getting reminder
                finish();
            }
        }

        try {
            Date reminderDate = Constant.DB_formatter.parse(reminder.getTime());
            reminderTime = reminderDate.getTime();
            dateAndTimeGC.setGregorianChange(reminderDate);
            setReminderContentTOUI();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        setUpToolbar();


        //reminderTypeGroup.setOnCheckedChangeListener(reminderTypeChangeListener);
        reminderTypeGroup.setEnabled(false);
        timePickerButton.setOnClickListener(this);
        datePickerButton.setOnClickListener(this);
        locationPickerButton.setOnClickListener(this);
        recipientsInput.addTextChangedListener(this);

        progressDialog = DialogManager.getProgressDialog(this,"Saveing Reminder");
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setReminderContentTOUI() throws ParseException {

        titleInput.setText(reminder.getTitle());
        descriptionInput.setText(reminder.getDescription());

        if (reminder.getType() == ProviderContract.ReminderContract.LOCATION_REMINDER){
            reminderLocationLayout.setVisibility(View.VISIBLE);
            locationPickerButton.setText(reminder.getAddress());
            reminderTypeGroup.check(R.id.new_reminder_item_location_reminder);
        }else {
            reminderTimeLayout.setVisibility(View.VISIBLE);
            CharSequence dateString = DateUtils.getRelativeDateTimeString(this, reminderTime, DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS, 0);
            datePickerButton.setText(dateString);
            reminderTypeGroup.check(R.id.new_reminder_item_time_reminder);
            timePickerButton.setText(Constant.ONLY_TIME_FORMATTER.format(reminderTime));
        }

        ArrayList<ContactDetail> contactDetails = Reminder.getParticipant(this.getApplicationContext(),reminder.getBackendReminderID());
        ContactDetail[] contactDetailsArray = new ContactDetail[contactDetails.size()];
        contactDetailsArray = contactDetails.toArray(contactDetailsArray);
        recipientsInput.setRecipients(contactDetailsArray);
    }

    private Reminder QueryReminder(Long reminderID) {
        return Reminder.getReminderByID(this.getApplicationContext(),reminderID);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_menu_save_item:


                if (checkReminderContentChanged()){
                    progressDialog.show();

                    editReminderObject();

                    //here we have the reminder object contain new info
                }

                return true;

            case android.R.id.home:
                if (checkReminderContentChanged())
                    DialogManager.ShowConfirmDialog(this);
                else
                    finish();
                return true;
        }

        return false;
    }

    private void editReminderObject() {
        reminder.setTitle(titleInput.getText().toString().trim());
        reminder.setDescription(descriptionInput.getText().toString().trim());
        if (participantChanged){
            newParticipants = recipientsInput.getRecipients();
        }
        reminder.setTime(Constant.DB_formatter.format(dateAndTimeGC.getTime()));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        ButterKnife.unbind(this);
    }


    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minutes) {
        dateAndTimeGC.set(GregorianCalendar.HOUR_OF_DAY, hourOfDay);
        dateAndTimeGC.set(GregorianCalendar.MINUTE, minutes);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minutes);
        timePickerButton.setText(Constant.ONLY_TIME_FORMATTER.format(calendar.getTime()));
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        CharSequence dateString = DateUtils.getRelativeDateTimeString(this, calendar.getTimeInMillis(), DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS,0);
        //String dateString = DateUtils.formatDateTime(getActivity(), calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_ABBREV_ALL);
        datePickerButton.setText(dateString.toString().split(",")[0]);
        dateAndTimeGC.set(GregorianCalendar.YEAR, year);
        dateAndTimeGC.set(GregorianCalendar.MONTH, monthOfYear);
        dateAndTimeGC.set(GregorianCalendar.DAY_OF_MONTH, dayOfMonth);
    }


    private RadioGroup.OnCheckedChangeListener reminderTypeChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i){
                case R.id.new_reminder_item_location_reminder:
                    reminderLocationLayout.setVisibility(View.VISIBLE);
                    reminderTimeLayout.setVisibility(View.GONE);
                    reminderType = LOCATION_REMINDER ;
                    break;

                case R.id.new_reminder_item_time_reminder:
                    reminderLocationLayout.setVisibility(View.GONE);
                    reminderTimeLayout.setVisibility(View.VISIBLE);
                    reminderType = TIME_REMINDER;
                    break;
            }
        }
    };


    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.new_reminder_title_input) EditText titleInput;
    @Bind(R.id.new_reminder_description_input) EditText descriptionInput;
    @Bind(R.id.new_reminder_recipients_input) ContactEditText recipientsInput;
    @Bind(R.id.new_reminder_type_radio_group) SegmentedGroup reminderTypeGroup;
    @Bind(R.id.new_reminder_pick_time) Button timePickerButton;
    @Bind(R.id.new_reminder_pick_date) Button datePickerButton;
    @Bind(R.id.new_reminder_pick_location) Button locationPickerButton;
    @Bind(R.id.new_reminder_time_layout) LinearLayout reminderTimeLayout;
    @Bind(R.id.new_reminder_location_layout) RelativeLayout reminderLocationLayout;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_reminder_pick_date:
                dateChanged = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                EditReminderActivity.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.show(getFragmentManager(), "Datepickerdialog");
                    }
                }, 300);
                break;

            case R.id.new_reminder_pick_time:
                timeChanged = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = now = Calendar.getInstance();
                        TimePickerDialog tpd = TimePickerDialog.newInstance(
                                EditReminderActivity.this,
                                now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE),
                                false
                        );
                        tpd.setThemeDark(false);
                        tpd.show(getFragmentManager(), "Timepickerdialog");
                    }
                }, 300);
                break;

            case R.id.new_reminder_pick_location:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        try {
                            startActivityForResult(builder.build(EditReminderActivity.this), PLACE_PICKER_REQUEST);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 400);
                break;

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                locationPickerButton.setText(place.getName());
                reminder.setLat(place.getLatLng().latitude);
                reminder.setLng(place.getLatLng().longitude);
                reminder.setAddress(place.getAddress().toString());
                locationChanged = true;
            }else
                locationChanged = false;
            Toast.makeText(this, getString(R.string.cancel_toast), Toast.LENGTH_LONG).show();
        }
    }


    private boolean checkReminderContentChanged(){
        return locationChanged ||
                dateChanged    ||
                timeChanged    ||
                titleInput.getText().toString().equals(reminder.getTitle()) ||
                descriptionInput.getText().toString().equals(reminder.getDescription()) ||
                participantChanged;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        participantChanged = true;
    }
}
