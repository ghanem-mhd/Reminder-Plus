package com.cnocompany.reminder.main_uI;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cnocompany.reminder.alarms_utilises.TimeAlarmManager;
import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.contacts.ContactsManager;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.helper.Constant;
import com.cnocompany.reminder.R;
import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.cnocompany.reminder.utilises.UtilisesPreferences;
import com.cnocompany.reminder.date_objects.Reminder;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ReminderViewerActivity extends AppCompatActivity{
    public static final int     LOCAL_REMINDER      = 1;
    public static final int     REMOTE_REMINDER     = 2;


    public static final String REMINDER_ARG      = "reminderArg";
    public static final String REMINDER_ID_ARG   = "reminderIDArg";
    public static final String REMINDER_TYPE_ARG = "reminderLocalOrServer";
    public static final String IS_NEWS_FEED_ARG  = "reminderISNewsArg";
    private static final int EDIT_REQ = 1;

    private Reminder viewedReminder;
    private int reminderType_Local_Server = 1;
    private boolean isNewsFeed;

    @Bind(R.id.full_reminder_toolbar) Toolbar toolbar;
    @Bind(R.id.full_reminder_title)         TextView  reminderTitle;
    @Bind(R.id.full_reminder_description)   TextView  reminderDescription;
    @Bind(R.id.full_reminder_time)          TextView  reminderTime;
    @Bind(R.id.full_reminder_location)      TextView reminderLocation;

    @Bind(R.id.full_reminder_participators) LinearLayout participatorsLayout;
    @Bind(R.id.full_reminder_time_layout) LinearLayout timeLayout;
    @Bind(R.id.full_reminder_location_layout) LinearLayout locationLayout;

    private LayoutInflater layoutInflater;
    private UtilisesPreferences utilisesPreferences;
    private UserInfoPreferences userInfoPreferences;
    private long userBackendID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.full_reminder_layout);
        ButterKnife.bind(this);
        layoutInflater =(LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        if (getIntent() != null){
            viewedReminder = (Reminder) getIntent().getSerializableExtra(REMINDER_ARG);
            if (viewedReminder == null){ //this mean that the caller send only the id
                long reminderID = getIntent().getLongExtra(REMINDER_ID_ARG,-1);
                if (reminderID == -1)
                    finish();
                else
                    queryReminder(reminderID);
            }
            reminderType_Local_Server = getIntent().getIntExtra(REMINDER_TYPE_ARG,1);
            isNewsFeed                = getIntent().getBooleanExtra(IS_NEWS_FEED_ARG,false);
        }else finish();

        setUpToolbar();

        changeIconColor();


        try {
            setReminderToUI();
            queryParticipant();
        } catch (ParseException e) {
            e.printStackTrace();
            finish();
        }

        utilisesPreferences = UtilisesPreferences.getInstance(this.getApplicationContext());
        userInfoPreferences = UserInfoPreferences.getInstance(this.getApplicationContext());
        userBackendID       = userInfoPreferences.getLong(UserInfoPreferences.USER_BACKEND_ID_LONG,-1);
    }

    private void queryReminder(long reminderID) {
        Uri reminderUri = ContentUris.withAppendedId(ProviderContract.ReminderContract.CONTENT_URI, reminderID);
        Cursor cursor = getContentResolver().query(reminderUri,null,null,null,null);
        if (cursor.moveToFirst()){
            viewedReminder = Reminder.getReminderObjectFromCursor(cursor);
        }else
            finish();
    }

    private void changeIconColor() {
        int accentColor = Color.parseColor("#448AFF");
        ImageView fullReminderIcon1 = (ImageView) findViewById(R.id.full_reminder_icon_1);
        ImageView fullReminderIcon2 = (ImageView) findViewById(R.id.contact_viewer_icon_2);
        ImageView fullReminderIcon3 = (ImageView) findViewById(R.id.full_reminder_icon_3);
        ImageView fullReminderIcon4 = (ImageView) findViewById(R.id.full_reminder_icon_4);
        fullReminderIcon1.setColorFilter(accentColor);
        fullReminderIcon2.setColorFilter(accentColor);
        fullReminderIcon3.setColorFilter(accentColor);
        fullReminderIcon4.setColorFilter(accentColor);
    }

    private void setReminderToUI() throws ParseException {
        reminderTitle.setText(viewedReminder.getTitle());
        reminderDescription.setText(viewedReminder.getDescription());

        if (viewedReminder.getType() == ProviderContract.ReminderContract.TIME_REMINDER){
            timeLayout.setVisibility(View.VISIBLE);
            Date reminderDate =  Constant.DB_formatter.parse(viewedReminder.getTime());
            CharSequence datePartString = DateUtils.getRelativeDateTimeString(this, reminderDate.getTime(), DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS, DateUtils.FORMAT_ABBREV_WEEKDAY);
            reminderTime.setText(datePartString.toString());
        }else{
            locationLayout.setVisibility(View.VISIBLE);
        }
    }

    private void queryParticipant(){
        if (reminderType_Local_Server == LOCAL_REMINDER){
            String selection = ProviderContract.ContactReminderContract.BACKEND_REMINDER_ID + " = ?";
            Cursor cursor = getContentResolver().query(ProviderContract.ContactReminderContract.CONTENT_URI, null, selection, new String[]{viewedReminder.getBackendReminderID() + ""}, null);
            if (cursor.moveToFirst()){
                do{
                    addLocalParticipantItemToUI(cursor.getInt(cursor.getColumnIndex(ProviderContract.ContactReminderContract.BACKEND_CONTACT_ID)), false);
                }while (cursor.moveToNext());
            }
            cursor.close();
        }else {
            addRemoteParticipantItemToUi();
        }
    }

    private void addRemoteParticipantItemToUi() {

    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("");
    }

    private void addLocalParticipantItemToUI(long userID, boolean isCreator){
        View participantView = layoutInflater.inflate(R.layout.participant_item,null);
        participantView.setId((int) userID);
        ButterKnife.bind(participantView);

        TextView  participantName    = ButterKnife.findById(participantView,R.id.participant_item_name);
        TextView  participantStatus  = ButterKnife.findById(participantView,R.id.participant_item_status);
        ImageView participantPhoto   = ButterKnife.findById(participantView,R.id.participant_item_photo);


        if (isCreator){
            participantStatus.setVisibility(View.VISIBLE);
            participantStatus.setText("Creator");
        }

        ContactObject contactObject = ContactsManager.getContactByBackendID(getApplicationContext(), userID);
        if (contactObject != null) {
            participantName.setText(contactObject.getLocalName());
            Picasso.with(getApplicationContext()).load(contactObject.getPhotoPath()).into(participantPhoto);
            participatorsLayout.addView(participantView);
            participantView.setOnClickListener(onParticipantClickListener);
        }
        ButterKnife.unbind(participantView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (viewedReminder.getBackendCreatorID() != userBackendID){
            getMenuInflater().inflate(R.menu.reminder_viewer_menu,menu);
            if (viewedReminder.getIsSilent() == 1){
                menu.findItem(R.id.reminder_view_menu_silent_item).setTitle(getString(R.string.un_silent_label));
            }else{
                menu.findItem(R.id.reminder_view_menu_silent_item).setTitle(getString(R.string.silent_label));

            }
        }else{
            getMenuInflater().inflate(R.menu.reminder_edit_menu,menu);
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            case R.id.reminder_view_menu_cancel_item:
                TimeAlarmManager.removeReminderAlarm(this, viewedReminder.get_ID());
                getContentResolver().delete(ProviderContract.ReminderContract.CONTENT_URI, ProviderContract.ReminderContract._ID + " = ?", new String[]{viewedReminder.get_ID() + ""});
                utilisesPreferences.put(UtilisesPreferences.Key.MARKER_DELETED,viewedReminder.get_ID());
                finish();
                break;

            case R.id.reminder_view_menu_silent_item:
                if (viewedReminder.getIsSilent() == 1 ){
                    changeSilent(0);
                    viewedReminder.setIsSilent(0);
                    item.setTitle(getString(R.string.silent_label));
                }else{
                    viewedReminder.setIsSilent(0);
                    changeSilent(1);
                    item.setTitle(getString(R.string.un_silent_label));
                }
                break;

            case R.id.reminder_edit_menu_edit_item:
                Intent intent = new Intent(this,EditReminderActivity.class);
                intent.putExtra(EditReminderActivity.REMINDER_ID_ARG,viewedReminder.get_ID());
                startActivityForResult(intent,EDIT_REQ);
                Toast.makeText(this,"must make it",Toast.LENGTH_LONG).show();


                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeSilent(int isSilent) {
        TimeAlarmManager.makeAlarmSilent(this, viewedReminder.get_ID(), isSilent);
        if (isSilent == 1){
            Toast.makeText(this, "Reminder marked as silent", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this, "Reminder marked as un-silent", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private View.OnClickListener onParticipantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("test", String.valueOf(view.getId()));
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
