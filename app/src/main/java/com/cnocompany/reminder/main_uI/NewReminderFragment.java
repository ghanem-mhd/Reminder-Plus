package com.cnocompany.reminder.main_uI;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.android.ex.chips.BaseRecipientAdapter;
import com.cnocompany.reminder.authenticator.AccountConstants;
import com.cnocompany.reminder.helper.Constant;

import com.cnocompany.reminder.helper.ContactDetail;
import com.cnocompany.reminder.helper.ContactEditText;
import com.cnocompany.reminder.R;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.rey.material.widget.Button;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.hoang8f.android.segmented.SegmentedGroup;


public class NewReminderFragment extends Fragment implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener{
    private static final String LOG_TAG = "RP_new_reminder";

    private static final int    LOCATION_REMINDER          = 0;
    private static final int    TIME_REMINDER              = 1;
    private static final int    PLACE_PICKER_REQUEST = 1;


    private static final SimpleDateFormat formatter = new SimpleDateFormat(Constant.DateTime_Format_IN_DB, Locale.getDefault());
    private static final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();


    @Bind(R.id.new_reminder_title_input) EditText titleInput;
    @Bind(R.id.new_reminder_description_input) EditText descriptionInput;
    @Bind(R.id.new_reminder_recipients_input) ContactEditText recipientsInput;
    @Bind(R.id.new_reminder_type_radio_group) SegmentedGroup reminderTypeGroup;
    @Bind(R.id.new_reminder_pick_time) Button timePickerButton;
    @Bind(R.id.new_reminder_pick_date) Button datePickerButton;
    @Bind(R.id.new_reminder_pick_location) Button locationPickerButton;
    @Bind(R.id.new_reminder_time_layout) LinearLayout reminderTimeLayout;
    @Bind(R.id.new_reminder_location_layout) RelativeLayout reminderLocationLayout;





    GregorianCalendar dateAndTimeGC;
    LatLng ReminderLocation = null;





    private boolean timeSelected,dateSelected,locationSelect;
    private int reminderType;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateAndTimeGC = new GregorianCalendar();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.reminder_editor_layout, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        setRecipientsInputData();

        reminderTypeGroup.setOnCheckedChangeListener(reminderTypeChangeListener);
        timePickerButton.setOnClickListener(this);
        datePickerButton.setOnClickListener(this);
        locationPickerButton.setOnClickListener(this);

    }

    private void showProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.new_reminder_loading_title));
        progressDialog.setMessage(getString(R.string.new_reminder_waiting));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_reminder_pick_date:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                NewReminderFragment.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                    }
                }, 300);
                break;

            case R.id.new_reminder_pick_time:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = now = Calendar.getInstance();
                        TimePickerDialog tpd = TimePickerDialog.newInstance(
                                NewReminderFragment.this,
                                now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE),
                                false
                        );
                        tpd.setThemeDark(false);
                        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                    }
                }, 300);
                break;

            case R.id.new_reminder_pick_location:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        try {
                            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 400);
                break;

        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minutes) {
        dateAndTimeGC.set(GregorianCalendar.HOUR_OF_DAY, hourOfDay);
        dateAndTimeGC.set(GregorianCalendar.MINUTE, minutes);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minutes);
        timePickerButton.setText(Constant.ONLY_TIME_FORMATTER.format(calendar.getTime()));
        timeSelected = true;
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        CharSequence dateString = DateUtils.getRelativeDateTimeString(getActivity(), calendar.getTimeInMillis(), DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS,0);
        //String dateString = DateUtils.formatDateTime(getActivity(), calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_ABBREV_ALL);
        datePickerButton.setText(dateString.toString().split(",")[0]);
        dateAndTimeGC.set(GregorianCalendar.YEAR, year);
        dateAndTimeGC.set(GregorianCalendar.MONTH, monthOfYear);
        dateAndTimeGC.set(GregorianCalendar.DAY_OF_MONTH, dayOfMonth);
        dateSelected = true;
    }

    private void clearInput() {
        titleInput.getText().clear();
        descriptionInput.getText().clear();
        recipientsInput.getText().clear();
        timePickerButton.setText(R.string.new_reminder_fragment_time_label);
        datePickerButton.setText(R.string.new_reminder_fragment_date_label);
        locationPickerButton.setText(R.string.new_reminder_fragment_location_label);

    }

    private void setRecipientsInputData(){
        BaseRecipientAdapter adapter = new BaseRecipientAdapter(BaseRecipientAdapter.QUERY_TYPE_PHONE, getActivity(),8);
        Account[] accounts = AccountManager.get(getActivity()).getAccountsByType(AccountConstants.ACCOUNT_TYPE);
        adapter.setAccount(accounts[0]);
        adapter.setShowMobileOnly(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu,menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_menu_save_item:

                if(validInput()){
                    sendReminder();
                }
                else
                    Toast.makeText(getActivity(),R.string.new_reminder_input_warning,Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendReminder() {
        showProgressDialog();
        String title                       = titleInput.getText().toString().trim();
        String description                 = descriptionInput.getText().toString().trim();
        ArrayList<String> fullPhoneNumber  = getPhonesNumber();
        String alarmTime                   = formatter.format(dateAndTimeGC.getTime());


        Log.d(LOG_TAG,title);
        Log.d(LOG_TAG,description);
        Log.d(LOG_TAG,fullPhoneNumber.toString());
        Log.d(LOG_TAG, alarmTime);

        clearInput();
    }

    private boolean validInput() {
        if (TextUtils.isEmpty(titleInput.getText()))
            return false;
        if (recipientsInput.getRecipients().length == 0)
            return false;
        if (reminderType == TIME_REMINDER && (!timeSelected || ! dateSelected))
            return false;
        return !(reminderType == LOCATION_REMINDER && !locationSelect);
    }

    //this method return selected phone number (14 columns)
    private ArrayList<String> getPhonesNumber(){
        ArrayList<String> fullPhoneNumbers = new ArrayList<>();
        ContactDetail[] contactDetails = recipientsInput.getRecipients();
        for (ContactDetail contactDetail : contactDetails) {
            fullPhoneNumbers.add(contactDetail.getPhone());
           /* try {

                //todo after use contactEditView we don't need this
                //Phonenumber.PhoneNumber number = phoneUtil.parse(recipient.getValue().toString(),getUserCountry());
                //Log.d(LOG_TAG,"Number obtained "+ number.toString());
                //Log.d(LOG_TAG,"full number "+ number.getCountryCode()+""+number.getNationalNumber());

            } catch (NumberParseException e) { e.printStackTrace(); }*/
        }
        return fullPhoneNumbers;
    }

    private String getUserCountry() {
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        return   telephonyManager.getSimCountryIso().toUpperCase();
    }

    private RadioGroup.OnCheckedChangeListener reminderTypeChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i){
                case R.id.new_reminder_item_location_reminder:
                    reminderLocationLayout.setVisibility(View.VISIBLE);
                    reminderTimeLayout.setVisibility(View.GONE);
                    reminderType = LOCATION_REMINDER ;
                break;

                case R.id.new_reminder_item_time_reminder:
                    reminderLocationLayout.setVisibility(View.GONE);
                    reminderTimeLayout.setVisibility(View.VISIBLE);
                    reminderType = TIME_REMINDER;
                break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                locationPickerButton.setText(place.getName());
                ReminderLocation = place.getLatLng();
                locationSelect = true;
            }else
                locationSelect = false;
                Toast.makeText(getActivity(),getString(R.string.cancel_toast),Toast.LENGTH_LONG).show();
        }
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
