package com.cnocompany.reminder.main_uI;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.cnocompany.reminder.authenticator.AccountConstants;
import com.cnocompany.reminder.utilises.UtilisesPreferences;

public class DispatchActivity extends Activity {
    private static final String LOG_TAG = "RP_dispatch";
    private static final int INFO_REQ = 1;
    private AccountManager accountManager ;
    private UtilisesPreferences utilisesPreferences;


    @Override
    final protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountManager = AccountManager.get(this);
        utilisesPreferences = UtilisesPreferences.getInstance(this);
        runDispatch();
    }


    private void runDispatch() {
        if (registered()){
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                finish();
        } else {
            addNewAccount(AccountConstants.ACCOUNT_TYPE, AccountConstants.AUTH_TOKEN_IND);
            finish();
        }
    }

    private boolean registered(){
        Account[] reminderPlusAccounts = accountManager.getAccountsByType(AccountConstants.ACCOUNT_TYPE);
        return reminderPlusAccounts.length > 0;
    }

    private boolean infoCompleted(){
        return utilisesPreferences.getBoolean(UtilisesPreferences.Key.INFO_DONE,false);
    }

    private void addNewAccount(String accountType, String authTokenType) {
        final AccountManagerFuture<Bundle> future = accountManager.addAccount(accountType, authTokenType, null, null, this, new AccountManagerCallback<Bundle>() {
            @Override
            public void run(AccountManagerFuture<Bundle> future) {
                try {
                    Bundle bnd = future.getResult();
                    Log.d(LOG_TAG, "Add New Account Bundle is " + bnd);
                    runDispatch();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, null);
    }

    private void requstUserInfo() {
        Intent intent = new Intent(DispatchActivity.this,EditUserInfoActivity.class);
        intent.putExtra(EditUserInfoActivity.REG_REQ_ARG, true);
        startActivityForResult(intent, INFO_REQ);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            utilisesPreferences.put(UtilisesPreferences.Key.INFO_DONE,true);
            runDispatch();
        }
    }
}
