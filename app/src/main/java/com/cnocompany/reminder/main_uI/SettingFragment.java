package com.cnocompany.reminder.main_uI;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cnocompany.reminder.R;
import com.cnocompany.reminder.contacts.ContactObject;
import com.cnocompany.reminder.contacts.ContactsLoader;
import com.cnocompany.reminder.contacts.ContactsManager;
import com.cnocompany.reminder.content_provider.ProviderContract;
import com.cnocompany.reminder.date_objects.Reminder;
import com.cnocompany.reminder.utilises.UserInfoPreferences;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class SettingFragment extends Fragment implements ContactsLoader.OnContactsLoaderFinish {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        new ContactsLoader(this).execute(this.getActivity().getApplicationContext());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.setting_fragment, container, false);




        return view;
    }


    @Override
    public void onFinish(ArrayList<ContactObject> contactObjects) {
        for (int i = 0; i < contactObjects.size(); i++) {
            ContactObject contactObject = contactObjects.get(i);
            contactObject.setIsUsingReminderPlus(true);
            contactObject.setBackendID(i);
            contactObject.setRemoteName("Mohammad " + i);
            contactObject.setPhotoPath(Uri.parse("http://185.3.152.227/api/image/user/" + 5));

            ContactsManager.addContact(getActivity(), contactObject);
        }


        UserInfoPreferences.getInstance(this.getContext()).put(UserInfoPreferences.USER_BACKEND_ID_LONG,120l);

        final Reminder reminder = new Reminder();


        reminder.setTitle("Home!");
        reminder.setDescription("test sdlk klka dfdkj adfkj dffasdf adfklj ");
        reminder.setTime("2015-10-11 15:10");
        reminder.setType(ProviderContract.ReminderContract.TIME_REMINDER);

        reminder.setIsSilent(1);
        reminder.setLat(33.430163);
        reminder.setLng(36.156304);
        reminder.setRadius(500);


        reminder.setAddress("Damasucse");
        reminder.setBackendCreatorID(120);
        reminder.setBackendReminderID(601);
        reminder.setBackendUserReminderID(10);
        reminder.setParticipantIDS(1l);
        reminder.setStatus(1);


        Reminder.AddReminderToContentProvider(getActivity(), reminder);
    }



}
